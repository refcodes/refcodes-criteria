module org.refcodes.criteria {
	requires org.refcodes.data;
	requires org.refcodes.textual;
	requires transitive org.refcodes.schema;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;
	requires org.refcodes.runtime;

	exports org.refcodes.criteria;
}
