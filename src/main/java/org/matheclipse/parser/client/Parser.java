/*
 * Copyright 2005-2008 Axel Kramer (axelclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client;

import org.matheclipse.parser.client.ast.ASTNode;
import org.matheclipse.parser.client.ast.FunctionNode;
import org.matheclipse.parser.client.ast.IConstantOperators;
import org.matheclipse.parser.client.ast.IParserFactory;
import org.matheclipse.parser.client.ast.NumberNode;
import org.matheclipse.parser.client.ast.SymbolNode;
import org.matheclipse.parser.client.operator.ASTNodeFactory;
import org.matheclipse.parser.client.operator.InfixOperator;
import org.matheclipse.parser.client.operator.Operator;
import org.matheclipse.parser.client.operator.PostfixOperator;
import org.matheclipse.parser.client.operator.PrefixOperator;

/**
 * Create an expression of the <code>ASTNode</code> class-hierarchy from a math
 * formulas string representation See
 * <a href="http://en.wikipedia.org/wiki/Operator-precedence_parser">Operator
 * -precedence parser</a> for the idea, how to parse the operators depending on
 * their precedence.
 */
public class Parser extends Scanner {
	/**
	 * Use '('...')' as brackets for arguments
	 */
	private final boolean fRelaxedSyntax;

	/**
	 * Instantiates a new parser.
	 */
	public Parser() {
		this( ASTNodeFactory.MMA_STYLE_FACTORY, false );
	}

	/**
	 * Instantiates a new parser.
	 * 
	 * @param relaxedSyntax if <code>true</code>, use '('...')' as brackets for
	 *        arguments
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError the syntax error
	 */
	public Parser( final boolean relaxedSyntax ) {
		this( ASTNodeFactory.MMA_STYLE_FACTORY, relaxedSyntax );
	}

	/**
	 * Instantiates a new parser.
	 * 
	 * @param factory the factory
	 * @param relaxedSyntax if <code>true</code>, use '('...')' as brackets for
	 *        arguments
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError the syntax error
	 */
	public Parser( IParserFactory factory, final boolean relaxedSyntax ) {
		this.fRelaxedSyntax = relaxedSyntax;
		this.fFactory = factory;
	}

	/**
	 * Sets the factory.
	 * 
	 * @param factory the new factory
	 */
	public void setFactory( final IParserFactory factory ) {
		this.fFactory = factory;
	}

	/**
	 * Gets the factory.
	 * 
	 * @return the factory
	 */
	public IParserFactory getFactory() {
		return fFactory;
	}

	/**
	 * construct the arguments for an expression.
	 * 
	 * @param function the function
	 * 
	 * @return the arguments
	 * 
	 * @throws SyntaxError the syntax error
	 */
	private void getArguments( final FunctionNode function ) {
		do {
			function.add( parseOperators( parsePrimary(), 0 ) );

			if ( fToken != TT_COMMA ) {
				break;
			}

			getNextToken();
		} while ( true );
	}

	/**
	 * Determine the current PrefixOperator.
	 * 
	 * @return <code>null</code> if no prefix operator could be determined
	 */
	private PrefixOperator determinePrefixOperator() {
		Operator oper = null;
		for ( Operator aFOperList : fOperList ) {
			oper = aFOperList;
			if ( oper instanceof PrefixOperator ) {
				return (PrefixOperator) oper;
			}
		}
		return null;
	}

	/**
	 * Determine the current PostfixOperator.
	 * 
	 * @return <code>null</code> if no postfix operator could be determined
	 */
	private PostfixOperator determinePostfixOperator() {
		Operator oper = null;
		for ( Operator aFOperList : fOperList ) {
			oper = aFOperList;
			if ( oper instanceof PostfixOperator ) {
				return (PostfixOperator) oper;
			}
		}
		return null;
	}

	/**
	 * Determine the current BinaryOperator.
	 * 
	 * @return <code>null</code> if no binary operator could be determined
	 */
	private InfixOperator determineBinaryOperator() {
		Operator oper = null;
		for ( Operator aFOperList : fOperList ) {
			oper = aFOperList;
			if ( oper instanceof InfixOperator ) {
				return (InfixOperator) oper;
			}
		}
		return null;
	}

	/**
	 * Parses the arguments.
	 * 
	 * @param lhs the lhs
	 * 
	 * @return the AST node
	 */
	private ASTNode parseArguments( ASTNode lhs ) {
		if ( fRelaxedSyntax ) {
			if ( fToken == TT_PRECEDENCE_OPEN ) {
				lhs = getFunction( lhs );
			}
		}
		else {
			if ( fToken == TT_ARGUMENTS_OPEN ) {
				lhs = getFunction( lhs );
			}
		}
		return lhs;
	}

	/**
	 * Parses the primary.
	 * 
	 * @return the AST node
	 */
	private ASTNode parsePrimary() {
		if ( fToken == TT_OPERATOR ) {
			final PrefixOperator prefixOperator = determinePrefixOperator();
			if ( prefixOperator != null ) {
				getNextToken();
				final ASTNode temp = parseLookaheadOperator( prefixOperator.getPrecedence() );
				if ( "PreMinus".equals( prefixOperator.getFunctionName() ) ) {
					// https://www.metacodes.pro cases for negative numbers
					if ( temp instanceof NumberNode ) {
						( (NumberNode) temp ).toggleSign();
						return temp;
					}
				}
				return prefixOperator.createFunction( fFactory, temp );
			}
			throwSyntaxError( "Operator: " + fOperatorString + " is no prefix operator." );

		}
		return getPart();
	}

	/**
	 * Parses the lookahead operator.
	 * 
	 * @param min_precedence the min precedence
	 * 
	 * @return the AST node
	 */
	private ASTNode parseLookaheadOperator( final int min_precedence ) {
		ASTNode rhs = parsePrimary();
		while ( true ) {
			final int lookahead = fToken;
			if ( ( fToken == TT_LIST_OPEN ) || ( fToken == TT_PRECEDENCE_OPEN ) || ( fToken == TT_IDENTIFIER ) || ( fToken == TT_STRING ) || ( fToken == TT_DIGIT ) ) {
				// lazy evaluation of multiplication
				final InfixOperator timesOperator = (InfixOperator) fFactory.get( "Times" );
				if ( timesOperator.getPrecedence() > min_precedence ) {
					rhs = parseOperators( rhs, timesOperator.getPrecedence() );
					continue;
				}
				else if ( ( timesOperator.getPrecedence() == min_precedence ) && ( timesOperator.getGrouping() == InfixOperator.RIGHT_ASSOCIATIVE ) ) {
					rhs = parseOperators( rhs, timesOperator.getPrecedence() );
					continue;
				}
			}
			else {
				if ( lookahead != TT_OPERATOR ) {
					break;
				}
				final InfixOperator infixOperator = determineBinaryOperator();
				if ( infixOperator != null ) {
					if ( infixOperator.getPrecedence() > min_precedence ) {
						rhs = parseOperators( rhs, infixOperator.getPrecedence() );
						continue;
					}
					else if ( ( infixOperator.getPrecedence() == min_precedence ) && ( infixOperator.getGrouping() == InfixOperator.RIGHT_ASSOCIATIVE ) ) {
						rhs = parseOperators( rhs, infixOperator.getPrecedence() );
						continue;
					}
				}
				else {
					final PostfixOperator postfixOperator = determinePostfixOperator();
					if ( postfixOperator != null ) {
						if ( postfixOperator.getPrecedence() > min_precedence ) {
							getNextToken();
							rhs = postfixOperator.createFunction( fFactory, rhs );
							continue;
						}
					}
				}
			}
			break;
		}
		return rhs;
	}

	/**
	 * See <a href=
	 * "http://en.wikipedia.org/wiki/Operator-precedence_parser">Operator
	 * -precedence parser</a> for the idea, how to parse the operators depending
	 * on their precedence.
	 * 
	 * @param lhs the already parsed left-hand-side of the operator
	 * @param min_precedence the min precedence
	 * 
	 * @return the AST node
	 */
	private ASTNode parseOperators( ASTNode lhs, final int min_precedence ) {
		ASTNode rhs = null;
		Operator oper;
		InfixOperator infixOperator;
		PostfixOperator postfixOperator;
		while ( true ) {
			if ( ( fToken == TT_LIST_OPEN ) || ( fToken == TT_PRECEDENCE_OPEN ) || ( fToken == TT_IDENTIFIER ) || ( fToken == TT_STRING ) || ( fToken == TT_DIGIT ) ) {
				// lazy evaluation of multiplication
				oper = fFactory.get( "Times" );
				if ( oper.getPrecedence() >= min_precedence ) {
					rhs = parseLookaheadOperator( oper.getPrecedence() );
					lhs = fFactory.createFunction( fFactory.createSymbol( oper.getFunctionName() ), lhs, rhs );
					lhs = parseArguments( lhs );
					continue;
				}
			}
			else {
				if ( fToken != TT_OPERATOR ) {
					break;
				}
				infixOperator = determineBinaryOperator();

				if ( infixOperator != null ) {
					if ( infixOperator.getPrecedence() >= min_precedence ) {
						getNextToken();
						rhs = parseLookaheadOperator( infixOperator.getPrecedence() );
						lhs = infixOperator.createFunction( fFactory, lhs, rhs );
						lhs = parseArguments( lhs );
						continue;
					}
				}
				else {
					postfixOperator = determinePostfixOperator();

					if ( postfixOperator != null ) {
						getNextToken();
						lhs = postfixOperator.createFunction( fFactory, lhs );
						lhs = parseArguments( lhs );
						continue;
					}
					throwSyntaxError( "Operator: " + fOperatorString + " is no infix or postfix operator." );
				}
			}
			break;
		}
		return lhs;
	}

	/**
	 * Parse the given <code>expression</code> String into an ASTNode.
	 * 
	 * @param expression a formula string which should be parsed.
	 * 
	 * @return the parsed ASTNode representation of the given formula string
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError the syntax error
	 */
	public ASTNode parse( final String expression ) {
		initialize( expression );
		final ASTNode temp = parseOperators( parsePrimary(), 0 );
		if ( fToken != TT_EOF ) {
			if ( fToken == TT_PRECEDENCE_CLOSE ) {
				throwSyntaxError( "Too many closing ')'; End-of-file not reached." );
			}
			if ( fToken == TT_LIST_CLOSE ) {
				throwSyntaxError( "Too many closing '}'; End-of-file not reached." );
			}
			if ( fToken == TT_ARGUMENTS_CLOSE ) {
				throwSyntaxError( "Too many closing ']'; End-of-file not reached." );
			}

			throwSyntaxError( "End-of-file not reached." );
		}

		return temp;
	}

	/**
	 * Method Declaration.
	 * 
	 * @param negative the negative
	 * 
	 * @return the number
	 * 
	 * @throws SyntaxError the syntax error
	 * 
	 * @see
	 */
	private ASTNode getNumber( final boolean negative ) {
		ASTNode temp = null;
		final Object[] result = getNumberString();
		String number = (String) result[0];
		final int numFormat = ( (Integer) result[1] ).intValue();
		try {
			if ( negative ) {
				number = '-' + number;
			}
			if ( numFormat < 0 ) {
				temp = fFactory.createDouble( number );
			}
			else {
				temp = fFactory.createInteger( number, numFormat );
			}
		}
		catch ( final Throwable e ) {
			throwSyntaxError( "Number format error: " + number, number.length() );
		}
		getNextToken();
		return temp;
	}

	/**
	 * Gets the integer number.
	 * 
	 * @return the integer number
	 * 
	 * @throws SyntaxError the syntax error
	 */
	private int getIntegerNumber() {
		final Object[] result = getNumberString();
		final String number = (String) result[0];
		final int numFormat = ( (Integer) result[1] ).intValue();
		int intValue = 0;
		try {
			intValue = Integer.parseInt( number, numFormat );
		}
		catch ( final NumberFormatException e ) {
			throwSyntaxError( "Number format error (not an int type): " + number, number.length() );
		}
		getNextToken();
		return intValue;
	}

	/**
	 * Read the current identifier from the expression factories table.
	 * 
	 * @return the symbol
	 * 
	 * @throws SyntaxError the syntax error
	 * 
	 * @see
	 */
	private SymbolNode getSymbol() {
		final String identifier = getIdentifier();
		if ( !fFactory.isValidIdentifier( identifier ) ) {
			throwSyntaxError( "Invalid identifier: " + identifier + " detected." );
		}

		final SymbolNode symbol = fFactory.createSymbol( identifier );
		getNextToken();
		return symbol;
	}

	/**
	 * Gets the string.
	 * 
	 * @return the string
	 * 
	 * @throws SyntaxError the syntax error
	 */
	private ASTNode getString() {
		final StringBuffer ident = getStringBuffer();

		getNextToken();

		return fFactory.createString( ident );
	}

	/**
	 * Get a list {...}
	 * 
	 * @return the list
	 * 
	 * @throws SyntaxError the syntax error
	 */
	private ASTNode getList() {
		final FunctionNode function = fFactory.createFunction( fFactory.createSymbol( IConstantOperators.List ) );

		getNextToken();

		if ( fToken == TT_LIST_CLOSE ) {
			getNextToken();

			return function;
		}

		getArguments( function );

		if ( fToken == TT_LIST_CLOSE ) {
			getNextToken();

			return function;
		}

		throwSyntaxError( "')' expected." );
		return null;
	}

	/**
	 * Get a function f[...][...]
	 * 
	 * @param head the head
	 * 
	 * @return the function
	 * 
	 * @throws SyntaxError the syntax error
	 */
	FunctionNode getFunction( final ASTNode head ) {
		final FunctionNode function = fFactory.createAST( head );

		getNextToken();

		if ( fRelaxedSyntax ) {
			if ( fToken == TT_PRECEDENCE_CLOSE ) {
				getNextToken();
				if ( fToken == TT_PRECEDENCE_OPEN ) {
					return getFunction( function );
				}
				return function;
			}
		}
		else {
			if ( fToken == TT_ARGUMENTS_CLOSE ) {
				getNextToken();
				if ( fToken == TT_ARGUMENTS_OPEN ) {
					return getFunction( function );
				}
				return function;
			}
		}
		getArguments( function );

		if ( fRelaxedSyntax ) {
			if ( fToken == TT_PRECEDENCE_CLOSE ) {
				getNextToken();
				if ( fToken == TT_PRECEDENCE_OPEN ) {
					return getFunction( function );
				}
				return function;
			}
		}
		else {
			if ( fToken == TT_ARGUMENTS_CLOSE ) {
				getNextToken();
				if ( fToken == TT_ARGUMENTS_OPEN ) {
					return getFunction( function );
				}
				return function;
			}
		}

		throwSyntaxError( "']' expected." );
		return null;
	}

	/**
	 * Gets the factor.
	 * 
	 * @return the factor
	 * 
	 * @throws SyntaxError the syntax error
	 */
	private ASTNode getFactor() {
		final ASTNode temp;

		if ( fToken == TT_IDENTIFIER ) {
			final SymbolNode symbol = getSymbol();

			if ( fToken == TT_BLANK ) {
				getNextToken();
				if ( fToken == TT_IDENTIFIER ) {
					final ASTNode check = getSymbol();
					return fFactory.createPattern( symbol, check );
				}
				else {
					return fFactory.createPattern( symbol, null );
				}
			}

			return symbol;
		}
		if ( fToken == TT_BLANK ) {
			getNextToken();
			if ( fToken == TT_IDENTIFIER ) {
				final ASTNode check = getSymbol();
				return fFactory.createPattern( null, check );
			}
			else {
				return fFactory.createPattern( null, null );
			}
		}
		if ( fToken == TT_DIGIT ) {
			return getNumber( false );
		}
		if ( fToken == TT_PRECEDENCE_OPEN ) {
			getNextToken();

			temp = parseOperators( parsePrimary(), 0 );

			if ( fToken != TT_PRECEDENCE_CLOSE ) {
				throwSyntaxError( "\')\' expected." );
			}

			getNextToken();

			return temp;
		}
		if ( fToken == TT_LIST_OPEN ) {
			return getList();
		}
		if ( fToken == TT_STRING ) {
			return getString();
		}
		if ( fToken == TT_PERCENT ) {

			final FunctionNode out = fFactory.createFunction( fFactory.createSymbol( IConstantOperators.Out ) );

			int countPercent = 1;
			getNextToken();
			if ( fToken == TT_DIGIT ) {
				countPercent = getIntegerNumber();
				out.add( fFactory.createInteger( countPercent ) );
				return out;
			}

			while ( fToken == TT_PERCENT ) {
				countPercent++;
				getNextToken();
			}

			out.add( fFactory.createInteger( -countPercent ) );
			return out;
		}
		if ( fToken == TT_SLOT ) {

			getNextToken();
			final FunctionNode slot = fFactory.createFunction( fFactory.createSymbol( IConstantOperators.Slot ) );
			if ( fToken == TT_DIGIT ) {
				slot.add( getNumber( false ) );
			}
			else {
				slot.add( fFactory.createInteger( 1 ) );
			}
			return slot;

		}
		if ( fToken == TT_SLOTSEQUENCE ) {

			getNextToken();
			final FunctionNode slotSequencce = fFactory.createFunction( fFactory.createSymbol( IConstantOperators.SlotSequence ) );
			if ( fToken == TT_DIGIT ) {
				slotSequencce.add( getNumber( false ) );
			}
			else {
				slotSequencce.add( fFactory.createInteger( 1 ) );
			}
			return slotSequencce;

		}
		switch ( fToken ) {
		case TT_PRECEDENCE_CLOSE -> throwSyntaxError( "Too much open ) in factor." );
		case TT_LIST_CLOSE -> throwSyntaxError( "Too much open } in factor." );
		case TT_ARGUMENTS_CLOSE -> throwSyntaxError( "Too much open ] in factor." );
		}

		throwSyntaxError( "Error in factor at character: '" + fCurrentChar + "' (" + fToken + ")" );
		return null;
	}

	/**
	 * Get a <i>part [[..]]</i> of an expression <code>{a,b,c}[[2]]</code>
	 * &rarr; <code>b</code>
	 * 
	 * @return the part
	 * 
	 * @throws SyntaxError the syntax error
	 */
	private ASTNode getPart() {
		ASTNode temp = getFactor();
		temp = parseArguments( temp );

		if ( fRelaxedSyntax ) {
			if ( fToken != TT_ARGUMENTS_OPEN ) {
				return temp;
			}
		}
		else {
			if ( fToken != TT_PARTOPEN ) {
				return temp;
			}
		}

		FunctionNode function = null;

		do {
			if ( function == null ) {
				function = fFactory.createFunction( fFactory.createSymbol( IConstantOperators.Part ) );
				function.add( temp );
			}
			else {
				function = fFactory.createFunction( fFactory.createSymbol( IConstantOperators.Part ), function );
			}

			getNextToken();

			if ( fRelaxedSyntax ) {
				if ( fToken == TT_ARGUMENTS_CLOSE ) {
					throwSyntaxError( "Statement (i.e. index) expected in [ ]." );
				}
			}
			else {
				if ( fToken == TT_ARGUMENTS_CLOSE ) {
					throwSyntaxError( "Statement (i.e. index) expected in [[ ]]." );
				}
			}

			function.add( parseOperators( parsePrimary(), 0 ) );

			if ( fRelaxedSyntax ) {
				// no action
			}
			else {
				if ( fToken == TT_ARGUMENTS_CLOSE ) {
					// scanner-step begin: (instead of getNextToken() call):
					if ( fInputString.length() > fCurrentPosition ) {
						if ( fInputString.charAt( fCurrentPosition ) == ']' ) {
							fCurrentPosition++;
							fToken = TT_PARTCLOSE;
						}
					}
					// scanner-step end
				}
			}
			if ( fRelaxedSyntax ) {
				if ( fToken != TT_ARGUMENTS_CLOSE ) {
					throwSyntaxError( "']' expected." );
				}
			}
			else {
				if ( fToken != TT_PARTCLOSE ) {
					throwSyntaxError( "']]' expected." );
				}
			}
			getNextToken();
		} while ( fToken == TT_PARTOPEN );

		return function;
	}
}
