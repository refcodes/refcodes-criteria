/*
 * Copyright 2005-2008 Axel Kramer (axelclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client;

import java.util.List;

import org.matheclipse.parser.client.ast.IParserFactory;
import org.matheclipse.parser.client.operator.Operator;

/**
 * The Class Scanner.
 */
public class Scanner {

	protected String fInputString;

	protected char fCurrentChar;

	protected int fCurrentPosition;

	protected int fToken;

	protected String fOperatorString;

	protected List<Operator> fOperList;

	/**
	 * Row counter for syntax errors.
	 */
	protected int rowCount;

	protected int fCurrentColumnStartPosition;

	/** Token type: End-of_File. */
	public static final int TT_EOF = 0;

	/** Token type: floating point number. */
	public static final int TT_FLOATING_POINT = 10;

	/** Token type: opening bracket for function arguments. */
	public static final int TT_ARGUMENTS_OPEN = 12;

	/** Token type: closing bracket for function arguments. */
	public static final int TT_ARGUMENTS_CLOSE = 13;

	/**
	 * Token type: opening bracket '(' for sub-formulas with higher precedence.
	 */
	public static final int TT_PRECEDENCE_OPEN = 14;

	/**
	 * Token type: closing bracket ')' for sub-formulas with higher precedence.
	 */
	public static final int TT_PRECEDENCE_CLOSE = 15;

	/** Token type: opening curly braces '{' for starting lists. */
	public static final int TT_LIST_OPEN = 16;

	/** Token type: closing curly braces '}' for ending lists. */
	public static final int TT_LIST_CLOSE = 17;

	/**
	 * Token type: opening brackets for starting the &quot;index part&quot; of
	 * an expression.
	 */
	public static final int TT_PARTOPEN = 18;

	/**
	 * Token type: closing brackets for ending the &quot;index part&quot; of an
	 * expression.
	 */
	public static final int TT_PARTCLOSE = 19;

	/** Token type: operator found in input string. */
	public static final int TT_OPERATOR = 31;

	/** ',' operator. */
	public static final int TT_COMMA = 134;

	/** '%' operator. */
	public static final int TT_PERCENT = 135;

	/**
	 * Token type: string surrounded by &quot;....&quot;
	 */
	public static final int TT_STRING = 136;

	/** Token type: pattern placeholder '_'. */
	public static final int TT_BLANK = 137;

	/** Token type: identifier name. */
	public static final int TT_IDENTIFIER = 138;

	/** Token type: digit 0,1,2,3,4,5,6,7,8,9. */
	public static final int TT_DIGIT = 139;

	/** Token type: slot #. */
	public static final int TT_SLOT = 141;

	/** Token type: slot sequence ##. */
	public static final int TT_SLOTSEQUENCE = 142;

	// ----------------optimized identifier managment------------------
	static final String string_a = "a";

	static final String string_b = "b";

	static final String string_c = "c";

	static final String string_d = "d";

	static final String string_e = "e";

	static final String string_f = "f";

	static final String string_g = "g";

	static final String string_h = "h";

	static final String string_i = "i";

	static final String string_j = "j";

	static final String string_k = "k";

	static final String string_l = "l";

	static final String string_m = "m";

	static final String string_n = "n";

	static final String string_o = "o";

	static final String string_p = "p";

	static final String string_q = "q";

	static final String string_r = "r";

	static final String string_s = "s";

	static final String string_t = "t";

	static final String string_u = "u";

	static final String string_v = "v";

	static final String string_w = "w";

	static final String string_x = "x";

	static final String string_y = "y";

	static final String string_z = "z";

	static final String var_a = "$a";

	static final String var_b = "$b";

	static final String var_c = "$c";

	static final String var_d = "$d";

	static final String var_e = "$e";

	static final String var_f = "$f";

	static final String var_g = "$g";

	static final String var_h = "$h";

	static final String var_i = "$i";

	static final String var_j = "$j";

	static final String var_k = "$k";

	static final String var_l = "$l";

	static final String var_m = "$m";

	static final String var_n = "$n";

	static final String var_o = "$o";

	static final String var_p = "$p";

	static final String var_q = "$q";

	static final String var_r = "$r";

	static final String var_s = "$s";

	static final String var_t = "$t";

	static final String var_u = "$u";

	static final String var_v = "$v";

	static final String var_w = "$w";

	static final String var_x = "$x";

	static final String var_y = "$y";

	static final String var_z = "$z";

	protected int numFormat = 0;

	protected IParserFactory fFactory;

	/**
	 * Initialize Scanner without a math-expression.
	 */
	public Scanner() {
		initializeNullScanner();
	}

	/**
	 * Initialize.
	 * 
	 * @param s the s
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError the syntax error
	 */
	protected void initialize( final String s ) {
		initializeNullScanner();
		fInputString = s;
		if ( s != null ) {
			getNextToken();
		}
	}

	/**
	 * Initialize null scanner.
	 */
	private void initializeNullScanner() {
		fInputString = null;
		fToken = TT_EOF;
		fCurrentPosition = 0;
		rowCount = 0;
		fCurrentColumnStartPosition = 0;
	}

	/**
	 * get the next Character from the input string.
	 * 
	 * @return the char
	 */
	private void getChar() {
		if ( fInputString.length() > fCurrentPosition ) {
			fCurrentChar = fInputString.charAt( fCurrentPosition++ );
			return;
		}

		fCurrentPosition = fInputString.length() + 1;
		fCurrentChar = ' ';
		fToken = TT_EOF;
	}

	/**
	 * getOperator.
	 * 
	 * @return the operator
	 */
	protected List<Operator> getOperator() {
		final int startPosition = fCurrentPosition - 1;
		fOperatorString = fInputString.substring( startPosition, fCurrentPosition );
		List<Operator> list = fFactory.getOperatorList( fOperatorString );
		List<Operator> lastList = null;
		int lastOperatorPosition = -1;
		if ( list != null ) {
			lastList = list;
			lastOperatorPosition = fCurrentPosition;
		}
		getChar();
		while ( fFactory.getOperatorCharacters().indexOf( fCurrentChar ) >= 0 ) {
			fOperatorString = fInputString.substring( startPosition, fCurrentPosition );
			list = fFactory.getOperatorList( fOperatorString );
			if ( list != null ) {
				lastList = list;
				lastOperatorPosition = fCurrentPosition;
			}
			getChar();
		}
		if ( lastOperatorPosition > 0 ) {
			fCurrentPosition = lastOperatorPosition;
			return lastList;
		}
		final int endPosition = fCurrentPosition--;
		fCurrentPosition = startPosition;
		throwSyntaxError( "Operator token not found: " + fInputString.substring( startPosition, endPosition - 1 ) );
		return null;
	}

	/**
	 * getNextToken
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError if any.
	 */
	protected void getNextToken() {

		while ( fInputString.length() > fCurrentPosition ) {
			fCurrentChar = fInputString.charAt( fCurrentPosition++ );
			fToken = TT_EOF;

			if ( fFactory.getOperatorCharacters().indexOf( fCurrentChar ) >= 0 ) {
				fOperList = getOperator();
				fToken = TT_OPERATOR;
				return;
			}

			if ( ( fCurrentChar != '\t' ) && ( fCurrentChar != '\r' ) && ( fCurrentChar != ' ' ) ) {
				if ( fCurrentChar == '\n' ) {
					rowCount++;
					fCurrentColumnStartPosition = fCurrentPosition;
					continue; // while loop
				}
				if ( ( ( fCurrentChar >= 'a' ) && ( fCurrentChar <= 'z' ) ) || ( ( fCurrentChar >= 'A' ) && ( fCurrentChar <= 'Z' ) ) || ( fCurrentChar == '$' ) ) {
					fToken = TT_IDENTIFIER;

					return;
				}
				if ( ( fCurrentChar >= '0' ) && ( fCurrentChar <= '9' ) ) {
					fToken = TT_DIGIT;

					return;
				}
				if ( fCurrentChar == '(' ) {
					if ( fInputString.length() > fCurrentPosition ) {
						if ( fInputString.charAt( fCurrentPosition ) == '*' ) {
							final int startPosition = fCurrentPosition;
							fCurrentPosition++;
							// read multiline comment until end of line:
							try {
								while ( true ) {
									if ( fInputString.charAt( fCurrentPosition ) == '*' && fInputString.charAt( fCurrentPosition + 1 ) == ')' ) {
										fCurrentPosition++;
										fCurrentPosition++;
										break;
									}
									fCurrentPosition++;
								}
							}
							catch ( IndexOutOfBoundsException ioobe ) {
								fCurrentPosition = startPosition;
								throwSyntaxError( "Comment doesn't end with '*)' (open multiline comment)" );
							}

							continue;
						}
					}
				}

				switch ( fCurrentChar ) {
				case '(' -> fToken = TT_PRECEDENCE_OPEN;
				case ')' -> fToken = TT_PRECEDENCE_CLOSE;
				case '{' -> fToken = TT_LIST_OPEN;
				case '}' -> fToken = TT_LIST_CLOSE;
				case '[' -> {
					fToken = TT_ARGUMENTS_OPEN;
					if ( fInputString.length() > fCurrentPosition ) {
						if ( fInputString.charAt( fCurrentPosition ) == '[' ) {
							fCurrentPosition++;
							fToken = TT_PARTOPEN;
							break;
						}
					}
				}
				case ']' -> fToken = TT_ARGUMENTS_CLOSE;
				case ',' -> fToken = TT_COMMA;
				case '_' -> fToken = TT_BLANK;
				case '.' -> {
					// token = TT_DOT;
					if ( fInputString.length() > fCurrentPosition ) {
						if ( ( fInputString.charAt( fCurrentPosition ) >= '0' ) && ( fInputString.charAt( fCurrentPosition ) <= '9' ) ) {
							// don't increment fCurrentPosition (see
							// getNumberString())
							// fCurrentPosition++;
							fToken = TT_DIGIT; // floating-point number
							break;
						}
					}
				}
				case '"' -> fToken = TT_STRING;
				case '%' -> fToken = TT_PERCENT;
				case '#' -> {
					fToken = TT_SLOT;
					if ( fInputString.length() > fCurrentPosition ) {
						if ( fInputString.charAt( fCurrentPosition ) == '#' ) {
							fCurrentPosition++;
							fToken = TT_SLOTSEQUENCE;

							break;
						}
					}
				}
				default -> throwSyntaxError( "unexpected character: '" + fCurrentChar + "'" );
				}

				if ( fToken == TT_EOF ) {
					throwSyntaxError( "token not found" );
				}

				return;
			}
		}

		fCurrentPosition = fInputString.length() + 1;
		fCurrentChar = ' ';
		fToken = TT_EOF;
	}

	/**
	 * Throw syntax error.
	 * 
	 * @param error the error
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError the syntax error
	 */
	protected void throwSyntaxError( final String error ) {
		throw new SyntaxError( fCurrentPosition - 1, rowCount, fCurrentPosition - fCurrentColumnStartPosition, getErrorLine(), error, 1 );
	}

	/**
	 * Throw syntax error.
	 * 
	 * @param error the error
	 * @param errorLength the error length
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError the syntax error
	 */
	protected void throwSyntaxError( final String error, final int errorLength ) {
		throw new SyntaxError( fCurrentPosition - errorLength, rowCount, fCurrentPosition - fCurrentColumnStartPosition, getErrorLine(), error, errorLength );
	}

	/**
	 * Gets the error line.
	 * 
	 * @return the error line
	 */
	private String getErrorLine() {
		if ( fInputString.length() < fCurrentPosition ) {
			fCurrentPosition--;
		}
		// read until end-of-line after the current fError
		int eol = fCurrentPosition;
		while ( fInputString.length() > eol ) {
			fCurrentChar = fInputString.charAt( eol++ );
			if ( fCurrentChar == '\n' ) {
				eol--;
				break;
			}
		}
		final String line = fInputString.substring( fCurrentColumnStartPosition, eol );
		return line;
	}

	/**
	 * Gets the identifier.
	 * 
	 * @return the identifier
	 */
	protected String getIdentifier() {
		final int startPosition = fCurrentPosition - 1;

		getChar();
		if ( fCurrentChar == '$' ) {
			getChar();
		}
		while ( ( ( fCurrentChar >= 'a' ) && ( fCurrentChar <= 'z' ) ) || ( ( fCurrentChar >= 'A' ) && ( fCurrentChar <= 'Z' ) ) || ( ( fCurrentChar >= '0' ) && ( fCurrentChar <= '9' ) ) ) {
			getChar();
		}

		int endPosition = fCurrentPosition--;
		final int length = ( --endPosition ) - startPosition;
		if ( length == 1 ) {
			return optimizedCurrentTokenSource1( startPosition, endPosition );
		}
		if ( length == 2 && fInputString.charAt( startPosition ) == '$' ) {
			return optimizedCurrentTokenSource2( startPosition, endPosition );
		}

		return fInputString.substring( startPosition, endPosition );
	}

	/**
	 * Optimized current token source 1.
	 * 
	 * @param startPosition the start position
	 * @param endPosition the end position
	 * 
	 * @return the string
	 */
	private final String optimizedCurrentTokenSource1( final int startPosition, final int endPosition ) {
		// return always the same String build only once

		return switch ( fInputString.charAt( startPosition ) ) {
		case 'a' -> string_a;
		case 'b' -> string_b;
		case 'c' -> string_c;
		case 'd' -> string_d;
		case 'e' -> string_e;
		case 'f' -> string_f;
		case 'g' -> string_g;
		case 'h' -> string_h;
		case 'i' -> string_i;
		case 'j' -> string_j;
		case 'k' -> string_k;
		case 'l' -> string_l;
		case 'm' -> string_m;
		case 'n' -> string_n;
		case 'o' -> string_o;
		case 'p' -> string_p;
		case 'q' -> string_q;
		case 'r' -> string_r;
		case 's' -> string_s;
		case 't' -> string_t;
		case 'u' -> string_u;
		case 'v' -> string_v;
		case 'w' -> string_w;
		case 'x' -> string_x;
		case 'y' -> string_y;
		case 'z' -> string_z;
		default -> fInputString.substring( startPosition, endPosition );
		};
	}

	/**
	 * Optimized current token source 2.
	 * 
	 * @param startPosition the start position
	 * @param endPosition the end position
	 * 
	 * @return the string
	 */
	private final String optimizedCurrentTokenSource2( final int startPosition, final int endPosition ) {
		// return always the same String build only once
		return switch ( fInputString.charAt( startPosition + 1 ) ) {
		case 'a' -> var_a;
		case 'b' -> var_b;
		case 'c' -> var_c;
		case 'd' -> var_d;
		case 'e' -> var_e;
		case 'f' -> var_f;
		case 'g' -> var_g;
		case 'h' -> var_h;
		case 'i' -> var_i;
		case 'j' -> var_j;
		case 'k' -> var_k;
		case 'l' -> var_l;
		case 'm' -> var_m;
		case 'n' -> var_n;
		case 'o' -> var_o;
		case 'p' -> var_p;
		case 'q' -> var_q;
		case 'r' -> var_r;
		case 's' -> var_s;
		case 't' -> var_t;
		case 'u' -> var_u;
		case 'v' -> var_v;
		case 'w' -> var_w;
		case 'x' -> var_x;
		case 'y' -> var_y;
		case 'z' -> var_z;
		default -> fInputString.substring( startPosition, endPosition );
		};
	}

	/**
	 * Gets the number string.
	 * 
	 * @return the number string
	 */
	protected Object[] getNumberString() {
		final Object[] result = new Object[2];
		numFormat = 10;
		int startPosition = fCurrentPosition - 1;
		final char firstCh = fCurrentChar;
		char dFlag = ' ';
		// first digit
		if ( fCurrentChar == '.' ) {
			dFlag = fCurrentChar;
		}
		getChar();
		if ( firstCh == '0' ) {
			switch ( fCurrentChar ) {
			case 'b' -> {
				numFormat = 2;
				startPosition = fCurrentPosition;
				getChar();
			}
			case 'B' -> {
				numFormat = 2;
				startPosition = fCurrentPosition;
				getChar();
			}
			case 'o' -> {
				numFormat = 8;
				startPosition = fCurrentPosition;
				getChar();
			}
			case 'O' -> {
				numFormat = 8;
				startPosition = fCurrentPosition;
				getChar();
			}
			case 'x' -> {
				numFormat = 16;
				startPosition = fCurrentPosition;
				getChar();
			}
			case 'X' -> {
				numFormat = 16;
				startPosition = fCurrentPosition;
				getChar();
			}
			}
		}

		if ( numFormat == 2 ) {
			while ( ( fCurrentChar >= '0' ) && ( fCurrentChar <= '1' ) ) {
				getChar();
			}
		}
		else if ( numFormat == 8 ) {
			while ( ( fCurrentChar >= '0' ) && ( fCurrentChar <= '7' ) ) {
				getChar();
			}
		}
		else if ( numFormat == 16 ) {
			while ( ( ( fCurrentChar >= '0' ) && ( fCurrentChar <= '9' ) ) || ( ( fCurrentChar >= 'a' ) && ( fCurrentChar <= 'f' ) ) || ( ( fCurrentChar >= 'A' ) && ( fCurrentChar <= 'F' ) ) ) {
				getChar();
			}
		}
		else {
			while ( ( ( fCurrentChar >= '0' ) && ( fCurrentChar <= '9' ) ) || ( fCurrentChar == '.' ) ) {
				// if ((ch == '.') || (ch == 'E') || (ch == 'e')) {
				if ( fCurrentChar == '.' ) {
					if ( ( fCurrentChar == '.' ) && ( dFlag != ' ' ) ) {
						break;
					}
					// if ((dFlag == 'E') || (dFlag == 'e')) {
					// break;
					// }
					dFlag = fCurrentChar;
					getChar();
					// if ((ch == '-') || (ch == '+')) {
					// getChar();
					// }
				}
				else {
					getChar();
				}
			}
			if ( dFlag != ' ' ) {
				numFormat = -1;
			}
		}
		if ( numFormat < 0 ) {
			if ( ( fCurrentChar == 'E' ) || ( fCurrentChar == 'e' ) ) {
				getChar();
				if ( ( fCurrentChar == '+' ) || ( fCurrentChar == '-' ) ) {
					getChar();
				}
				while ( ( ( fCurrentChar >= '0' ) && ( fCurrentChar <= '9' ) ) ) {
					getChar();
				}
			}
		}
		// }
		int endPosition = fCurrentPosition--;
		result[0] = fInputString.substring( startPosition, --endPosition );
		result[1] = Integer.valueOf( numFormat );
		return result;
	}

	/**
	 * Gets the string buffer.
	 * 
	 * @return the string buffer
	 * 
	 * @throws org.matheclipse.parser.client.SyntaxError the syntax error
	 */
	protected StringBuffer getStringBuffer() {
		final StringBuffer ident = new StringBuffer();

		getChar();

		if ( ( fCurrentChar == '\n' ) || ( fToken == TT_EOF ) ) {
			throwSyntaxError( "string -" + ident.toString() + "- contains no character." );
		}

		while ( fCurrentChar != '"' ) {
			if ( ( fCurrentChar == '\\' ) ) {
				getChar();

				switch ( fCurrentChar ) {
				case '\\' -> ident.append( fCurrentChar );
				case 'n' -> ident.append( "\n" );
				case 't' -> ident.append( "\t" );
				default -> throwSyntaxError( "string - unknown character after back-slash." );
				}

				getChar();
			}
			else {
				if ( ( fCurrentChar != '"' ) && ( ( fCurrentChar == '\n' ) || ( fToken == TT_EOF ) ) ) {
					throwSyntaxError( "string -" + ident.toString() + "- not closed." );
				}

				ident.append( fCurrentChar );
				getChar();
			}
		}

		return ident;
	}
}
