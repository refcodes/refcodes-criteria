/*
 * Copyright 2005-2008 Axel Kramer (axelclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client;

import org.matheclipse.parser.client.math.MathException;

/**
 * Exception for a syntax error detected by the MathEclipse parser.
 */
public class SyntaxError extends MathException {

	/** 
	 *
	 */
	private static final long serialVersionUID = 1849387697719679119L;

	int fStartOffset;

	int fRowIndex;

	/** column index where the error occurred (offset relative to rowIndex). */
	int fColumnIndex;

	int fLength;

	String fCurrentLine;

	String fError;

	/**
	 * SyntaxError exception.
	 * 
	 * @param startOffset the start offset
	 * @param rowIndx the row indx
	 * @param columnIndx the column indx
	 * @param currentLine the current line
	 * @param error the error
	 * @param length the length
	 */
	public SyntaxError( final int startOffset, final int rowIndx, final int columnIndx, final String currentLine, final String error, final int length ) {
		fStartOffset = startOffset;
		fRowIndex = rowIndx;
		fColumnIndex = columnIndx;
		fCurrentLine = currentLine;
		fError = error;
		fLength = length;
	}

	/**
	 * Gets the aMessage.
	 * 
	 * @return the aMessage
	 */
	@Override
	public String getMessage() {
		final StringBuilder buf = new StringBuilder( 256 );
		buf.append( "Syntax error in line: " );
		buf.append( fRowIndex + 1 );
		buf.append( " - " + fError + "\n" );
		buf.append( fCurrentLine + "\n" );
		for ( int i = 0; i < ( fColumnIndex - 1 ); i++ ) {
			buf.append( ' ' );
		}
		buf.append( '^' );
		return buf.toString();
	}

	/**
	 * offset where the error occurred.
	 * 
	 * @return the start offset
	 */
	public int getStartOffset() {
		return fStartOffset;
	}

	/**
	 * column index where the error occurred (offset relative to rowIndex).
	 * 
	 * @return the column index
	 */
	public int getColumnIndex() {
		return fColumnIndex;
	}

	/**
	 * source code line, where the error occurred.
	 * 
	 * @return the current line
	 */
	public String getCurrentLine() {
		return fCurrentLine;
	}

	/**
	 * the error string.
	 * 
	 * @return the error
	 */
	public String getError() {
		return fError;
	}

	/**
	 * length of the error.
	 * 
	 * @return the length
	 */
	public int getLength() {
		return fLength;
	}

	/**
	 * row index where the error occurred.
	 * 
	 * @return the row index
	 */
	public int getRowIndex() {
		return fRowIndex;
	}
}
