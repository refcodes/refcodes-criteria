/*
 * Copyright 2005-2008 Axel Kramer (axellclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client.ast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * A list of <code>ASTNode</code>'s which represents a parsed function. The head
 * of the function (i.e. Sin, Cos, Times,...) is stored in the 0-th index of the
 * list. The arguments of the function are stored in the 1...n-th index of the
 * list.
 */
public class FunctionNode extends ASTNode implements java.util.List<ASTNode> {

	private final ArrayList<ASTNode> fNodesList;

	/**
	 * Instantiates a new function node.
	 * 
	 * @param head the head
	 */
	public FunctionNode( final ASTNode head ) {
		super( null );
		fNodesList = new ArrayList<>( 5 );
		fNodesList.add( head );
	}

	/**
	 * Instantiates a new function node.
	 * 
	 * @param head the head
	 * @param arg0 the arg 0
	 */
	public FunctionNode( final SymbolNode head, final ASTNode arg0 ) {
		super( null );
		fNodesList = new ArrayList<>( 3 );
		fNodesList.add( head );
		fNodesList.add( arg0 );
	}

	/**
	 * Instantiates a new function node.
	 * 
	 * @param head the head
	 * @param arg0 the arg 0
	 * @param arg1 the arg 1
	 */
	public FunctionNode( final SymbolNode head, final ASTNode arg0, final ASTNode arg1 ) {
		super( null );
		fNodesList = new ArrayList<>( 3 );
		fNodesList.add( head );
		fNodesList.add( arg0 );
		fNodesList.add( arg1 );
	}

	/**
	 * Adds the.
	 * 
	 * @param index the index
	 * @param element the element
	 */
	@Override
	public void add( final int index, final ASTNode element ) {
		fNodesList.add( index, element );
	}

	/**
	 * Adds the.
	 * 
	 * @param e the e
	 * 
	 * @return a boolean.
	 */
	@Override
	public boolean add( final ASTNode e ) {
		return fNodesList.add( e );
	}

	/**
	 * {@inheritDoc} Adds the all.
	 */
	@Override
	public boolean addAll( final Collection<? extends ASTNode> c ) {
		return fNodesList.addAll( c );
	}

	/**
	 * {@inheritDoc} Adds the all.
	 */
	@Override
	public boolean addAll( final int index, final Collection<? extends ASTNode> c ) {
		return fNodesList.addAll( index, c );
	}

	/**
	 * Clear.
	 */
	@Override
	public void clear() {
		fNodesList.clear();
	}

	/**
	 * {@inheritDoc} Contains.
	 */
	@Override
	public boolean contains( final Object o ) {
		return fNodesList.contains( o );
	}

	/**
	 * {@inheritDoc} Contains all.
	 */
	@Override
	public boolean containsAll( final Collection<?> c ) {
		return fNodesList.containsAll( c );
	}

	/**
	 * Ensure capacity.
	 * 
	 * @param minCapacity the min capacity
	 */
	public void ensureCapacity( final int minCapacity ) {
		fNodesList.ensureCapacity( minCapacity );
	}

	/**
	 * {@inheritDoc} Equals.
	 */
	@Override
	public boolean equals( final Object o ) {
		if ( o instanceof FunctionNode ) {
			return fNodesList.equals( ( (FunctionNode) o ).fNodesList );
		}
		return false;
	}

	/**
	 * {@inheritDoc} Gets the.
	 */
	@Override
	public ASTNode get( final int index ) {
		return fNodesList.get( index );
	}

	/**
	 * Gets the node.
	 * 
	 * @param index the index
	 * 
	 * @return the node
	 */
	public ASTNode getNode( final int index ) {
		return fNodesList.get( index );
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 */
	@Override
	public int hashCode() {
		return fNodesList.hashCode();
	}

	/**
	 * {@inheritDoc} Index of.
	 */
	@Override
	public int indexOf( final Object o ) {
		return fNodesList.indexOf( o );
	}

	/**
	 * Checks if is empty.
	 * 
	 * @return a boolean.
	 */
	@Override
	public boolean isEmpty() {
		return fNodesList.isEmpty();
	}

	/**
	 * Iterator.
	 * 
	 * @return the iterator
	 */
	@Override
	public Iterator<ASTNode> iterator() {
		return fNodesList.iterator();
	}

	/**
	 * {@inheritDoc} Last index of.
	 */
	@Override
	public int lastIndexOf( final Object o ) {
		return fNodesList.lastIndexOf( o );
	}

	/**
	 * List iterator.
	 * 
	 * @return the list iterator
	 */
	@Override
	public ListIterator<ASTNode> listIterator() {
		return fNodesList.listIterator();
	}

	/**
	 * {@inheritDoc} List iterator.
	 */
	@Override
	public ListIterator<ASTNode> listIterator( final int index ) {
		return fNodesList.listIterator( index );
	}

	/**
	 * Removes the.
	 * 
	 * @param index the index
	 * 
	 * @return the AST node
	 */
	@Override
	public ASTNode remove( final int index ) {
		return fNodesList.remove( index );
	}

	/**
	 * {@inheritDoc} Removes the.
	 */
	@Override
	public boolean remove( final Object o ) {
		return fNodesList.remove( o );
	}

	/**
	 * {@inheritDoc} Removes the all.
	 */
	@Override
	public boolean removeAll( final Collection<?> c ) {
		return fNodesList.removeAll( c );
	}

	/**
	 * {@inheritDoc} Retain all.
	 */
	@Override
	public boolean retainAll( final Collection<?> c ) {
		return fNodesList.retainAll( c );
	}

	/**
	 * Sets the.
	 * 
	 * @param index the index
	 * @param element the element
	 * 
	 * @return the AST node
	 */
	@Override
	public ASTNode set( final int index, final ASTNode element ) {
		return fNodesList.set( index, element );
	}

	/**
	 * Size.
	 * 
	 * @return the int
	 */
	@Override
	public int size() {
		return fNodesList.size();
	}

	/**
	 * {@inheritDoc} Because GWT doesn't support the subList() method, we also
	 * throw an UnsupportedOperationException.
	 */
	@Override
	public List<ASTNode> subList( final int fromIndex, final int toIndex ) {
		throw new UnsupportedOperationException( "Arraylist#subList() not supported" );
	}

	/**
	 * To array.
	 * 
	 * @return the object[]
	 */
	@Override
	public Object[] toArray() {
		return fNodesList.toArray();
	}

	/**
	 * To array.
	 * 
	 * @param a the a
	 * 
	 * @return the object[]
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object[] toArray( final Object[] a ) {
		return fNodesList.toArray( a );
	}

	/**
	 * To string.
	 * 
	 * @return the string
	 */
	@Override
	public String toString() {
		ASTNode temp = fNodesList.get( 0 );
		final StringBuilder buf = new StringBuilder();
		if ( temp == null ) {
			buf.append( "&lt;null-tag&gt;" );
		}
		else {
			buf.append( temp.toString() );
		}
		buf.append( "[" );
		for ( int i = 1; i < size(); i++ ) {
			temp = get( i );
			buf.append( temp == this ? "(this ListNode)" : String.valueOf( temp ) );
			if ( i < size() - 1 ) {
				buf.append( ", " );
			}
		}
		buf.append( "]" );
		return buf.toString();
	}

	/**
	 * Trim to size.
	 */
	public void trimToSize() {
		fNodesList.trimToSize();
	}

	/**
	 * {@inheritDoc} Depends on.
	 */
	@Override
	public boolean dependsOn( String variableName ) {
		for ( int i = 1; i < size(); i++ ) {
			if ( get( i ).dependsOn( variableName ) ) {
				return true;
			}
		}
		return false;
	}
}
