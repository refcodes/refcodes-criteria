/*
 * Copyright 2005-2008 Axel Kramer (axellclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client.ast;

/**
 * The basic node for storing a parsed number expression string.
 */
public abstract class NumberNode extends ASTNode {

	protected boolean sign;

	/**
	 * Instantiates a new number node.
	 * 
	 * @param value the value
	 */
	protected NumberNode( final String value ) {
		super( value );
		sign = false;
	}

	/**
	 * Toggle sign.
	 */
	public void toggleSign() {
		sign = !sign;
	}

	/**
	 * Gets the string.
	 * 
	 * @return the string
	 */
	@Override
	public String getString() {
		if ( sign ) {
			return "-" + fStringValue;
		}
		return fStringValue;
	}

	/**
	 * To string.
	 * 
	 * @return the string
	 */
	@Override
	public String toString() {
		if ( sign ) {
			return "-" + fStringValue;
		}
		return fStringValue;
	}

	/**
	 * Checks if is sign.
	 * 
	 * @return a boolean.
	 */
	public boolean isSign() {
		return sign;
	}

	/**
	 * Double value.
	 * 
	 * @return the double
	 */
	public double doubleValue() {
		return Double.parseDouble( toString() );
	}

	/**
	 * {@inheritDoc} Equals.
	 */
	@Override
	public abstract boolean equals( Object obj );

	/**
	 * Hash code.
	 * 
	 * @return the int
	 */
	@Override
	public int hashCode() {
		if ( sign ) {
			return fStringValue.hashCode() * 17;
		}
		return fStringValue.hashCode();
	}
}
