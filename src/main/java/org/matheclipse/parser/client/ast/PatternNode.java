/*
 * Copyright 2005-2008 Axel Kramer (axellclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client.ast;

/**
 * A node for a parsed pattern expression (i.e. x_)
 */
public class PatternNode extends ASTNode {

	private final SymbolNode fSymbol;

	private final ASTNode fConstraint;

	/**
	 * Instantiates a new pattern node.
	 * 
	 * @param symbol the symbol
	 * @param constraint the constraint
	 */
	public PatternNode( final SymbolNode symbol, final ASTNode constraint ) {
		super( null );
		fSymbol = symbol;
		fConstraint = constraint;
	}

	/**
	 * Gets the constraint.
	 * 
	 * @return the constraint
	 */
	public ASTNode getConstraint() {
		return fConstraint;
	}

	/**
	 * Gets the symbol.
	 * 
	 * @return the symbol
	 */
	public SymbolNode getSymbol() {
		return fSymbol;
	}

	/**
	 * To string.
	 * 
	 * @return the string
	 */
	@Override
	public String toString() {
		final StringBuilder buff = new StringBuilder();
		if ( fSymbol != null ) {
			buff.append( fSymbol.toString() );
		}
		buff.append( "_" );
		if ( fConstraint != null ) {
			buff.append( fConstraint.toString() );
		}
		return buff.toString();
	}

	/**
	 * {@inheritDoc} Equals.
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( obj instanceof PatternNode pn ) {
			if ( fSymbol == pn.fSymbol ) {
				if ( fConstraint == null || pn.fConstraint == null ) {
					return fConstraint == pn.fConstraint;
				}
				return fConstraint.equals( pn.fConstraint );
			}
			else {
				if ( fSymbol == null || pn.fSymbol == null ) {
					return false;
				}
				if ( fSymbol.equals( pn.fSymbol ) ) {
					if ( fConstraint == null || pn.fConstraint == null ) {
						return fConstraint == pn.fConstraint;
					}
					return fConstraint.equals( pn.fConstraint );
				}
			}
		}
		return false;
	}

	/**
	 * Hash code.
	 * 
	 * @return the int
	 */
	@Override
	public int hashCode() {
		if ( fSymbol != null ) {
			fSymbol.hashCode();
		}
		return 0;
	}
}
