/*
 * Copyright 2005-2008 Axel Kramer (axellclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client.eval;

/**
 * The Class DoubleVariable.
 */
public class DoubleVariable implements IDoubleValue {

	double value;

	/**
	 * Instantiates a new double variable.
	 * 
	 * @param v the v
	 */
	public DoubleVariable( double v ) {
		value = v;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.matheclipse.parser.eval.IDoubleValue#getValue()
	 */
	@Override
	public double getValue() {
		return value;
	}

	/**
	 * {@inheritDoc} Sets the value.
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.matheclipse.parser.eval.IDoubleValue#setValue(double)
	 */
	@Override
	public void setValue( double value ) {
		this.value = value;
	}
}
