package org.matheclipse.parser.client.math;

/**
 * The Class MathException.
 */
public class MathException extends RuntimeException {

	private static final long serialVersionUID = 3520033778672500363L;

	/**
	 * Instantiates a new according exception.
	 */
	public MathException() {}

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public MathException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aMessage The aMessage describing this exception.
	 */
	public MathException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public MathException( Throwable aCause ) {
		super( aCause );
	}
}
