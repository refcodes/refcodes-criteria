/*
 * Copyright 2005-2008 Axel Kramer (axellclk@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/TEXT-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.matheclipse.parser.client.operator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.matheclipse.parser.client.ast.ASTNode;
import org.matheclipse.parser.client.ast.FloatNode;
import org.matheclipse.parser.client.ast.FractionNode;
import org.matheclipse.parser.client.ast.FunctionNode;
import org.matheclipse.parser.client.ast.IParserFactory;
import org.matheclipse.parser.client.ast.IntegerNode;
import org.matheclipse.parser.client.ast.PatternNode;
import org.matheclipse.parser.client.ast.StringNode;
import org.matheclipse.parser.client.ast.SymbolNode;

/**
 * A factory for creating ASTNode objects.
 */
public class ASTNodeFactory implements IParserFactory {

	public static final int PLUS_PRECEDENCE = 2900;

	public static final int TIMES_PRECEDENCE = 3800;

	public static final int POWER_PRECEDENCE = 5700;

	static final String[] HEADER_STRINGS = { "MapAll", "TimesBy", "Plus", "UpSet", "CompoundExpression", "Map", "Unset", "Apply", "ReplaceRepeated", "Less", "And", "Divide", "Set", "Increment", "Factorial2", "LessEqual", "NonCommutativeMultiply", "Factorial", "Times", "Power", "Dot", "Not", "PreMinus", "SameQ", "RuleDelayed", "GreaterEqual", "ArgsSyntax", "Colon", "DivideBy", "Or", "Equal", "StringJoin", "Unequal", "Decrement", "SubtractFrom", "PrePlus",
			// "RepeatedNull",
			"UnsameQ", "Rule", "UpSetDelayed", "PreIncrement", "Function", "Greater", "PreDecrement", "Subtract", "SetDelayed", "Alternatives", "AddTo",
			// "Repeated",
			"ReplaceAll" };

	static final String[] OPERATOR_STRINGS = { "//@", "*=", "+", "^=", ";", "/@", "=.", "@@", "//.", "<", "&&", "/", "=", "++", "!!", "<=", "**", "!", "*", "^", ".", "!", "-", "===", ":>", ">=", "/;", ":", "/=", "||", "==", "<>", "!=", "--", "-=", "+",
			// "...",
			"=!=", "->", "^;=", "++", "&", ">", "--", "-", ":=", "|", "+=",
			// "..",
			"/." };

	static final Operator[] OPERATORS = { new InfixOperator( "//@", "MapAll", 6100, InfixOperator.RIGHT_ASSOCIATIVE ), new InfixOperator( "*=", "TimesBy", 900, InfixOperator.NONE ), new InfixOperator( "+", "Plus", PLUS_PRECEDENCE, InfixOperator.NONE ), new InfixOperator( "^=", "UpSet", 300, InfixOperator.NONE ), new InfixOperator( ";", "CompoundExpression", 100, InfixOperator.NONE ), new InfixOperator( "/@", "Map", 6100, InfixOperator.RIGHT_ASSOCIATIVE ), new PostfixOperator( "=.", "Unset", 300 ), new InfixOperator( "@@", "Apply", 6100, InfixOperator.RIGHT_ASSOCIATIVE ), new InfixOperator( "//.", "ReplaceRepeated", 1000, InfixOperator.LEFT_ASSOCIATIVE ), new InfixOperator( "&lt;", "Less", 2600, InfixOperator.NONE ), new InfixOperator( "&&", "And", 2000, InfixOperator.NONE ), new DivideOperator( "/", "Divide", 4500, InfixOperator.LEFT_ASSOCIATIVE ), new InfixOperator( "=", "Set", 300, InfixOperator.RIGHT_ASSOCIATIVE ), new PostfixOperator( "++", "Increment", 6400 ),
			new PostfixOperator( "!!", "Factorial2", 6000 ), new InfixOperator( "&lt;=", "LessEqual", 2600, InfixOperator.NONE ), new InfixOperator( "**", "NonCommutativeMultiply", 5000, InfixOperator.NONE ), new PostfixOperator( "!", "Factorial", 6000 ), new InfixOperator( "*", "Times", TIMES_PRECEDENCE, InfixOperator.NONE ), new InfixOperator( "^", "Power", POWER_PRECEDENCE, InfixOperator.RIGHT_ASSOCIATIVE ), new InfixOperator( ".", "Dot", 4700, InfixOperator.NONE ), new PrefixOperator( "!", "Not", 2100 ), new PreMinusOperator( "-", "PreMinus", 4600 ), new InfixOperator( "===", "SameQ", 2400, InfixOperator.NONE ), new InfixOperator( ":>", "RuleDelayed", 1100, InfixOperator.RIGHT_ASSOCIATIVE ), new InfixOperator( "&gt;=", "GreaterEqual", 2600, InfixOperator.NONE ), new InfixOperator( "/;", "ArgsSyntax", 1200, InfixOperator.LEFT_ASSOCIATIVE ), new InfixOperator( ":", "Colon", 700, InfixOperator.NONE ), new InfixOperator( "/=", "DivideBy", 900, InfixOperator.NONE ),
			new InfixOperator( "||", "Or", 1900, InfixOperator.NONE ), new InfixOperator( "==", "Equal", 2600, InfixOperator.NONE ), new InfixOperator( "&lt;>", "StringJoin", 5800, InfixOperator.NONE ), new InfixOperator( "!=", "Unequal", 2600, InfixOperator.NONE ), new PostfixOperator( "--", "Decrement", 6400 ), new InfixOperator( "-=", "SubtractFrom", 900, InfixOperator.NONE ), new PrePlusOperator( "+", "PrePlus", 4600 ),
			// new PostfixOperator("...", "RepeatedNull", 1500),
			new InfixOperator( "=!=", "UnsameQ", 2400, InfixOperator.NONE ), new InfixOperator( "->", "Rule", 1100, InfixOperator.RIGHT_ASSOCIATIVE ), new InfixOperator( "^;=", "UpSetDelayed", 300, InfixOperator.NONE ), new PrefixOperator( "++", "PreIncrement", 6400 ), new PostfixOperator( "&", "Function", 800 ), new InfixOperator( "&gt;", "Greater", 2600, InfixOperator.NONE ), new PrefixOperator( "--", "PreDecrement", 6400 ), new SubtractOperator( "-", "Subtract", 2900, InfixOperator.LEFT_ASSOCIATIVE ), new InfixOperator( ":=", "SetDelayed", 300, InfixOperator.NONE ), new InfixOperator( "|", "Alternatives", 1400, InfixOperator.NONE ), new InfixOperator( "+=", "AddTo", 900, InfixOperator.NONE ),
			// new PostfixOperator("..", "Repeated", 1500),
			new InfixOperator( "/.", "ReplaceAll", 1000, InfixOperator.LEFT_ASSOCIATIVE ) };

	public static final ASTNodeFactory MMA_STYLE_FACTORY = new ASTNodeFactory();

	private static final HashMap<String, Operator> fOperatorMap;

	private static final HashMap<String, ArrayList<Operator>> fOperatorTokenStartSet;

	static {
		fOperatorMap = new HashMap<>();
		fOperatorTokenStartSet = new HashMap<>();
		for ( int i = 0; i < HEADER_STRINGS.length; i++ ) {
			addOperator( fOperatorMap, fOperatorTokenStartSet, OPERATOR_STRINGS[i], HEADER_STRINGS[i], OPERATORS[i] );
		}
	}

	/**
	 * Create a default ASTNode factory.
	 */
	public ASTNodeFactory() {}

	/**
	 * Adds the operator.
	 * 
	 * @param operatorMap the operator map
	 * @param operatorTokenStartSet the operator token start set
	 * @param operatorStr the operator str
	 * @param headStr the head str
	 * @param oper the oper
	 */
	public static void addOperator( final Map<String, Operator> operatorMap, final Map<String, ArrayList<Operator>> operatorTokenStartSet, final String operatorStr, final String headStr, final Operator oper ) {
		ArrayList<Operator> list;
		operatorMap.put( headStr, oper );
		list = operatorTokenStartSet.get( operatorStr );
		if ( list == null ) {
			list = new ArrayList<>( 2 );
			list.add( oper );
			operatorTokenStartSet.put( operatorStr, list );
		}
		else {
			list.add( oper );
		}
	}

	/**
	 * Gets the operator characters.
	 * 
	 * @return the operator characters
	 */
	@Override
	public String getOperatorCharacters() {
		return DEFAULT_OPERATOR_CHARACTERS;
	}

	/**
	 * Get identifier to operator map.
	 * 
	 * @return the identifier 2 operator map
	 */
	@Override
	public Map<String, Operator> getIdentifier2OperatorMap() {
		return fOperatorMap;
	}

	/**
	 * {@inheritDoc} Gets the.
	 */
	@Override
	public Operator get( final String identifier ) {
		return fOperatorMap.get( identifier );
	}

	/**
	 * Gets the operator 2 list map.
	 * 
	 * @return the operator 2 list map
	 */
	@Override
	public Map<String, ArrayList<Operator>> getOperator2ListMap() {
		return fOperatorTokenStartSet;
	}

	/**
	 * {@inheritDoc} Gets the operator list.
	 */
	@Override
	public List<Operator> getOperatorList( final String key ) {
		return fOperatorTokenStartSet.get( key );
	}

	/**
	 * Creates a new ASTNode object.
	 * 
	 * @param operatorStr the operator str
	 * @param headStr the head str
	 * @param precedence the precedence
	 * @param grouping the grouping
	 * 
	 * @return the infix operator
	 */
	public static InfixOperator createInfixOperator( final String operatorStr, final String headStr, final int precedence, final int grouping ) {
		final InfixOperator oper;
		if ( "Divide".equals( headStr ) ) {
			oper = new DivideOperator( operatorStr, headStr, precedence, grouping );
		}
		else if ( "Subtract".equals( headStr ) ) {
			oper = new SubtractOperator( operatorStr, headStr, precedence, grouping );
		}
		else {
			oper = new InfixOperator( operatorStr, headStr, precedence, grouping );
		}
		return oper;
	}

	/**
	 * Creates a new ASTNode object.
	 * 
	 * @param operatorStr the operator str
	 * @param headStr the head str
	 * @param precedence the precedence
	 * 
	 * @return the prefix operator
	 */
	public static PrefixOperator createPrefixOperator( final String operatorStr, final String headStr, final int precedence ) {
		final PrefixOperator oper;
		if ( "PreMinus".equals( headStr ) ) {
			oper = new PreMinusOperator( operatorStr, headStr, precedence );
		}
		else if ( "PrePlus".equals( headStr ) ) {
			oper = new PrePlusOperator( operatorStr, headStr, precedence );
		}
		else {
			oper = new PrefixOperator( operatorStr, headStr, precedence );
		}
		return oper;
	}

	/**
	 * Creates a new ASTNode object.
	 * 
	 * @param operatorStr the operator str
	 * @param headStr the head str
	 * @param precedence the precedence
	 * 
	 * @return the postfix operator
	 */
	public static PostfixOperator createPostfixOperator( final String operatorStr, final String headStr, final int precedence ) {
		return new PostfixOperator( operatorStr, headStr, precedence );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public ASTNode createDouble( final String doubleString ) {
		return new FloatNode( doubleString );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public FunctionNode createFunction( final SymbolNode head ) {
		return new FunctionNode( head );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public FunctionNode createFunction( final SymbolNode head, final ASTNode arg0 ) {
		return new FunctionNode( head, arg0 );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public FunctionNode createFunction( final SymbolNode head, final ASTNode arg0, final ASTNode arg1 ) {
		return new FunctionNode( head, arg0, arg1 );
	}

	/**
	 * {@inheritDoc} Creates a new list with no arguments from the given header
	 * object .
	 */
	@Override
	public FunctionNode createAST( final ASTNode headExpr ) {
		return new FunctionNode( headExpr );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public IntegerNode createInteger( final String integerString, final int numberFormat ) {
		return new IntegerNode( integerString, numberFormat );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public IntegerNode createInteger( final int intValue ) {
		return new IntegerNode( intValue );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public FractionNode createFraction( final IntegerNode numerator, final IntegerNode denominator ) {
		return new FractionNode( numerator, denominator );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public PatternNode createPattern( final SymbolNode patternName, final ASTNode check ) {
		return new PatternNode( patternName, check );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public StringNode createString( final StringBuffer buffer ) {
		return new StringNode( buffer.toString() );
	}

	/**
	 * {@inheritDoc} Creates a new ASTNode object.
	 */
	@Override
	public SymbolNode createSymbol( final String symbolName ) {
		return new SymbolNode( symbolName );
	}

	/**
	 * {@inheritDoc} Checks if is valid identifier.
	 */
	@Override
	public boolean isValidIdentifier( String identifier ) {
		return true;
	}
}
