// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import org.refcodes.schema.Schema;

/**
 * Base class with the base {@link org.refcodes.criteria.CriteriaLeaf}
 * functionality provided for {@link org.refcodes.criteria.CriteriaLeaf}
 * implementations.
 *
 * @author steiner
 * 
 * @version $Id: $Id
 * 
 * @param <T> the generic type
 */
public abstract class AbstractCriteriaLeaf<T> extends AbstractCriteria implements CriteriaLeaf<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _key;
	private T _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link org.refcodes.criteria.CriteriaLeaf} with the given
	 * name.
	 *
	 * @param aAlias The name of the {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public AbstractCriteriaLeaf( String aAlias ) {
		super( aAlias );
	}

	/**
	 * Constructs a {@link org.refcodes.criteria.CriteriaLeaf} with the given
	 * name, key and value.
	 *
	 * @param aAlias The name of the {@link org.refcodes.criteria.CriteriaLeaf}.
	 * @param aKey The value for the {@link org.refcodes.criteria.CriteriaLeaf}.
	 * @param aValue the value
	 */
	public AbstractCriteriaLeaf( String aAlias, String aKey, T aValue ) {
		super( aAlias );
		_key = aKey;
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getValue() {
		return _value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setKey( String aKey ) {
		_key = aKey;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue( T aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link CriteriaSchema} for a {@link CriteriaLeaf} sub-class
	 * with the instance's type, alias, key and value as well as the provided
	 * description.
	 * 
	 * @param aDescription The description to use for the {@link Schema}
	 *        instance.
	 * 
	 * @return The according {@link Schema} instance.
	 */
	protected CriteriaSchema toSchema( String aDescription ) {
		final CriteriaSchema theSchema = new CriteriaSchema( getAlias(), getClass(), getValue(), aDescription );
		theSchema.put( Schema.KEY, getKey() );
		return theSchema;
	}
}
