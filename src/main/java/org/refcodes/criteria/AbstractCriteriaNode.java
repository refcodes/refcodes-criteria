// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.refcodes.schema.Schema;

/**
 * Base class with the base functionality provided for
 * {@link org.refcodes.criteria.CriteriaNode} implementations.
 */
public abstract class AbstractCriteriaNode extends AbstractCriteria implements CriteriaNode {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected List<Criteria> _children;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link org.refcodes.criteria.CriteriaNode} with the given
	 * name.
	 *
	 * @param aAlias The name of the {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public AbstractCriteriaNode( String aAlias ) {
		super( aAlias );
		_children = new ArrayList<>();
	}

	/**
	 * Constructs a {@link org.refcodes.criteria.CriteriaNode} with the given
	 * name and the provided {@link org.refcodes.criteria.Criteria} children.
	 *
	 * @param aAlias The name of the {@link org.refcodes.criteria.CriteriaNode}.
	 * @param aChildren The {@link org.refcodes.criteria.Criteria} children to
	 *        be contained in the {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public AbstractCriteriaNode( String aAlias, Criteria... aChildren ) {
		super( aAlias );
		_children = new ArrayList<>();
		Collections.addAll( _children, aChildren );
	}

	/**
	 * Constructs a {@link org.refcodes.criteria.CriteriaNode} with the given
	 * name and the backing(!) {@link List} of the
	 * {@link org.refcodes.criteria.Criteria} children to be provided: The
	 * {@link #getChildren()} method will return the backing {@link List} as
	 * given, so you may provide an unmodifiable {@link List} if needed!
	 *
	 * @param aAlias The name of the {@link org.refcodes.criteria.CriteriaNode}.
	 * @param aChildren The backing(!) {@link List} of the
	 *        {@link org.refcodes.criteria.Criteria} children to be used by the
	 *        {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public AbstractCriteriaNode( String aAlias, List<Criteria> aChildren ) {
		super( aAlias );
		_children = aChildren;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addChild( Criteria aChild ) {
		_children.add( aChild );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Criteria> getChildren() {
		return _children;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link CriteriaSchema} for a {@link CriteriaNode} sub-class
	 * with the instance's type and alias as well as the provided description.
	 * 
	 * @param aDescription The description to use for the {@link Schema}
	 *        instance.
	 * 
	 * @return The according {@link Schema} instance.
	 */
	public CriteriaSchema toSchema( String aDescription ) {
		final CriteriaSchema theSchema = new CriteriaSchema( getAlias(), getClass(), aDescription );
		CriteriaSchema[] theSchemas = null;
		if ( _children != null && _children.size() != 0 ) {
			theSchemas = new CriteriaSchema[_children.size()];
			for ( int i = 0; i < theSchemas.length; i++ ) {
				theSchemas[i] = _children.get( i ).toSchema();
			}
		}
		return new CriteriaSchema( theSchema, theSchemas );
	}
}
