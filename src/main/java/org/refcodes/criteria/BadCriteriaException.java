// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * The {@link BadCriteriaException} is thrown in case of encountering bad
 * {@link Criteria} definitions.
 */
public class BadCriteriaException extends CriteriaException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public BadCriteriaException( String aMessage, Criteria aCriteria, String aErrorCode ) {
		super( aMessage, aCriteria, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BadCriteriaException( String aMessage, Criteria aCriteria, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCriteria, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BadCriteriaException( String aMessage, Criteria aCriteria, Throwable aCause ) {
		super( aMessage, aCriteria, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public BadCriteriaException( String aMessage, Criteria aCriteria ) {
		super( aMessage, aCriteria );
	}

	/**
	 * {@inheritDoc}
	 */
	public BadCriteriaException( Criteria aCriteria, Throwable aCause, String aErrorCode ) {
		super( aCriteria, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BadCriteriaException( Criteria aCriteria, Throwable aCause ) {
		super( aCriteria, aCause );
	}

	// /////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////

}