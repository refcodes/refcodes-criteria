// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * The Class ComplexCriteriaException.
 */
public class ComplexCriteriaException extends BadCriteriaException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCriteria the criteria
	 * @param aMessage The aMessage describing this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public ComplexCriteriaException( Criteria aCriteria, String aMessage, String aErrorCode ) {
		super( aMessage, aCriteria, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCriteria the criteria
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public ComplexCriteriaException( Criteria aCriteria, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCriteria, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCriteria the criteria
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public ComplexCriteriaException( Criteria aCriteria, String aMessage, Throwable aCause ) {
		super( aMessage, aCriteria, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCriteria the criteria
	 * @param aMessage The aMessage describing this exception.
	 */
	public ComplexCriteriaException( Criteria aCriteria, String aMessage ) {
		super( aMessage, aCriteria );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCriteria the criteria
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public ComplexCriteriaException( Criteria aCriteria, Throwable aCause, String aErrorCode ) {
		super( aCriteria, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCriteria the criteria
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public ComplexCriteriaException( Criteria aCriteria, Throwable aCause ) {
		super( aCriteria, aCause );
	}
}
