// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import org.refcodes.mixin.AliasAccessor;
import org.refcodes.schema.Schemable;

/**
 * The {@link org.refcodes.criteria.Criteria} itself is the base definition of
 * functionality which the {@link org.refcodes.criteria.CriteriaNode} and
 * {@link org.refcodes.criteria.CriteriaLeaf} implementations are to support.
 * Mainly, a {@link org.refcodes.criteria.Criteria} is to have a name (for
 * example "AND", "OR", "LESS_THAN" and so on). The
 * {@link org.refcodes.criteria.Criteria} instances can either be constructed in
 * an object oriented manner by instantiating the various criteria classes and
 * nesting them according to the required query or from a
 * {@link java.lang.String} by using the
 * {@link org.refcodes.criteria.ExpressionCriteriaFactory}. On the other hand a
 * {@link org.refcodes.criteria.Criteria} can be converted to a String to be
 * used in SQL statements by using the
 * {@link org.refcodes.criteria.ExpressionQueryFactory}.
 */
public interface Criteria extends AliasAccessor, Schemable {

	/**
	 * {@inheritDoc}
	 */
	@Override
	CriteriaSchema toSchema();
}
