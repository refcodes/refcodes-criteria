// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * Provides an accessor for a {@link org.refcodes.criteria.Criteria} property.
 */
public interface CriteriaAccessor {

	/**
	 * Retrieves the {@link org.refcodes.criteria.Criteria} from the
	 * {@link org.refcodes.criteria.Criteria} property.
	 *
	 * @return The {@link org.refcodes.criteria.Criteria} stored by the
	 *         {@link org.refcodes.criteria.Criteria} property.
	 */
	Criteria getCriteria();

	/**
	 * Provides a mutator for a {@link Criteria} property.
	 */
	public interface CriteriaMutator {

		/**
		 * Sets the {@link Criteria} for the {@link Criteria} property.
		 * 
		 * @param aCriteria The {@link Criteria} to be stored by the criteria
		 *        property.
		 */
		void setCriteria( Criteria aCriteria );
	}

	/**
	 * Provides a {@link Criteria} property.
	 */
	public interface CriteriaProperty extends CriteriaAccessor, CriteriaMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Criteria}
		 * (setter) as of {@link #setCriteria(Criteria)} and returns the very
		 * same value (getter).
		 * 
		 * @param aCriteria The {@link Criteria} to set (via
		 *        {@link #setCriteria(Criteria)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Criteria letCriteria( Criteria aCriteria ) {
			setCriteria( aCriteria );
			return aCriteria;
		}
	}
}
