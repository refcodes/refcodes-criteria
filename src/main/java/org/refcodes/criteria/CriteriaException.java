// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import org.refcodes.exception.AbstractException;

/**
 * The {@link CriteriaException} is the base {@link Exception} for
 * {@link Criteria} related problems. {@link Criteria}.
 */
public class CriteriaException extends AbstractException implements CriteriaAccessor {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////

	private final Criteria _criteria;

	// /////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCriteria The criteria involved in this exception.
	 */
	public CriteriaException( String aMessage, Criteria aCriteria, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_criteria = aCriteria;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCriteria The criteria involved in this exception.
	 */
	public CriteriaException( String aMessage, Criteria aCriteria, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_criteria = aCriteria;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCriteria The criteria involved in this exception.
	 */
	public CriteriaException( String aMessage, Criteria aCriteria, Throwable aCause ) {
		super( aMessage, aCause );
		_criteria = aCriteria;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCriteria The criteria involved in this exception.
	 */
	public CriteriaException( String aMessage, Criteria aCriteria ) {
		super( aMessage );
		_criteria = aCriteria;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCriteria The criteria involved in this exception.
	 */
	public CriteriaException( Criteria aCriteria, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_criteria = aCriteria;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aCriteria The criteria involved in this exception.
	 */
	public CriteriaException( Criteria aCriteria, Throwable aCause ) {
		super( aCause );
		_criteria = aCriteria;
	}

	// /////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Criteria getCriteria() {
		return _criteria;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _criteria };
	}
}