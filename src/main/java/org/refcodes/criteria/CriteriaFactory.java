// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import java.text.ParseException;

/**
 * The {@link org.refcodes.criteria.CriteriaFactory} constructs a
 * {@link org.refcodes.criteria.Criteria} (tree) from the provided query. The
 * syntax of the query is implementation specific and may look as follows: ( ( (
 * City = 'Berlin' ) OR ( City = 'Munich' ) ) AND ( Surname = 'Miller' ) )
 * CAUTION: The syntax supported for the query statement is implementation
 * depended! The {@link org.refcodes.criteria.ExpressionCriteriaFactory}
 * implements a {@link org.refcodes.criteria.CriteriaFactory} being capable of
 * parsing the above query.
 *
 * @author steiner
 * 
 * @version $Id: $Id
 * 
 * @param <Q> The type of the query statement, usually a
 *        {@link java.lang.String}.
 */
public interface CriteriaFactory<Q> {

	/**
	 * Parsed the given query and constructs a
	 * {@link org.refcodes.criteria.Criteria} (tree). Whether a
	 * {@link org.refcodes.criteria.CriteriaNode} or just a
	 * {@link org.refcodes.criteria.CriteriaLeaf} is returned depends on the
	 * complexity of the query.
	 *
	 * @param aQuery The query to be parsed.
	 * 
	 * @return A {@link org.refcodes.criteria.Criteria} (tree) representing the
	 *         query as an object oriented
	 *         {@link org.refcodes.criteria.Criteria} tree structure.
	 * 
	 * @throws ParseException the parse exception
	 */
	Criteria fromQuery( Q aQuery ) throws ParseException;
}
