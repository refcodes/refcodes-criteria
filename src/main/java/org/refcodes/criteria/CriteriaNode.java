// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import java.util.List;

/**
 * A CriteriaNode tree node may represent a logical AND or a logical OR or a
 * logical NOT applied on the node's children Criteria (CriteriaNode instances
 * and CriteriaLeaf instances).
 */
public interface CriteriaNode extends Criteria {

	/**
	 * Returns the list of child {@link org.refcodes.criteria.Criteria}
	 * instances contained in the {@link org.refcodes.criteria.CriteriaNode}.
	 *
	 * @return The {@link org.refcodes.criteria.Criteria} instances contained in
	 *         the {@link org.refcodes.criteria.CriteriaNode}.
	 */
	List<Criteria> getChildren();

	/**
	 * Adds a child {@link org.refcodes.criteria.Criteria} to the
	 * {@link org.refcodes.criteria.CriteriaNode}. In case a
	 * https://www.metacodes.proized {@link org.refcodes.criteria.CriteriaNode}
	 * applies constraints on the number or type of
	 * {@link org.refcodes.criteria.Criteria} which may be added, an according
	 * exception may be thrown. For example a single {@link CriteriaNode} may
	 * apply constraints on this method.
	 *
	 * @param aChild The child {@link org.refcodes.criteria.Criteria} to be
	 *        added.
	 * 
	 * @throws IllegalStateException the illegal state exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws java.lang.IllegalStateException In case constraints in terms of
	 *         state are violated, implementation depended.
	 * @throws java.lang.IllegalArgumentException In case constraints in terms
	 *         of argument are violated, implementation depended.
	 */
	void addChild( Criteria aChild );
}
