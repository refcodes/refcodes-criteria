// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import org.refcodes.mixin.DescriptionAccessor;
import org.refcodes.mixin.ValueAccessor;
import org.refcodes.schema.Schema;

/**
 * The purpose of a {@link CriteriaSchema} is automatically generate
 * documentation of {@link Criteria} structures.
 */
public class CriteriaSchema extends Schema implements DescriptionAccessor, ValueAccessor<Object> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CriteriaSchema( String aAlias, Class<?> aType, String aDescription, Schema... aChildren ) {
		super( aAlias, aType, aDescription, aChildren );
	}

	/**
	 * {@inheritDoc}
	 */
	public CriteriaSchema( CriteriaSchema aSchema, Schema... aChildren ) {
		super( aSchema, aChildren );
	}

	/**
	 * {@inheritDoc}
	 */
	public CriteriaSchema( Class<?> aType, CriteriaSchema aSchema ) {
		super( aType, aSchema );
	}

	/**
	 * Instantiates a new {@link CriteriaSchema}.
	 *
	 * @param aAlias The alias (name) of the schema described by the
	 *        {@link Schema} providing type.
	 * @param aType The type providing the this {@link Schema} instance.
	 * @param aValue The a value of the according element.
	 * @param aDescription The description of the schema described by the
	 *        {@link Schema} providing type.
	 */
	public CriteriaSchema( String aAlias, Class<?> aType, Object aValue, String aDescription ) {
		super( aAlias, aType, aDescription );
		put( VALUE, aValue );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getValue() {
		return get( VALUE );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

}