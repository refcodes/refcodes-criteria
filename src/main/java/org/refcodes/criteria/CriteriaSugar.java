// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions of {@link org.refcodes.criteria.Criteria}
 * trees: ... Criteria theCriteria = and( or( equalWith( "City", "Berlin" ),
 * equalWith( "City", "Munich" ) ), equalWith( "Surname", "Miller" ) ); ...
 */
public class CriteriaSugar {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Prevent instantiation as this is a utility class with static methods.
	 */
	private CriteriaSugar() {}

	// /////////////////////////////////////////////////////////////////////////
	// IMPORT STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an AND {@link org.refcodes.criteria.CriteriaNode} containing the
	 * provided {@link org.refcodes.criteria.Criteria} children.
	 *
	 * @param aChildren The {@link org.refcodes.criteria.Criteria} children to
	 *        be contained in the AND
	 *        {@link org.refcodes.criteria.CriteriaNode}.
	 * 
	 * @return The AND {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public static AndCriteria and( Criteria... aChildren ) {
		return new AndCriteria( aChildren );
	}

	/**
	 * Creates an EQUAL WITH ("=") {@link org.refcodes.criteria.CriteriaLeaf}
	 * expression relating to the given key and the given reference value.
	 *
	 * @param <T> The type of the {@link org.refcodes.criteria.CriteriaLeaf}'s
	 *        value.
	 * @param aKey The key on which the reference value is to be applied.
	 * @param aValue The reference value to be applied on the key.
	 * 
	 * @return The EQUAL WITH {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public static <T> EqualWithCriteria<T> equalWith( String aKey, T aValue ) {
		return new EqualWithCriteria<>( aKey, aValue );
	}

	/**
	 * Creates an GREATER OR EQUAL THAN ("&gt;=")
	 * {@link org.refcodes.criteria.CriteriaLeaf} expression relating to the
	 * given key and the given reference value.
	 *
	 * @param <T> The type of the {@link org.refcodes.criteria.CriteriaLeaf}'s
	 *        value.
	 * @param aKey The key on which the reference value is to be applied.
	 * @param aValue The reference value to be applied on the key.
	 * 
	 * @return The GREATER OR EQUAL THAN
	 *         {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public static <T> GreaterOrEqualThanCriteria<T> greaterOrEqualThan( String aKey, T aValue ) {
		return new GreaterOrEqualThanCriteria<>( aKey, aValue );
	}

	/**
	 * Creates an GREATER THAN ("&gt;")
	 * {@link org.refcodes.criteria.CriteriaLeaf} expression relating to the
	 * given key and the given reference value.
	 *
	 * @param <T> The type of the {@link org.refcodes.criteria.CriteriaLeaf}'s
	 *        value.
	 * @param aKey The key on which the reference value is to be applied.
	 * @param aValue The reference value to be applied on the key.
	 * 
	 * @return The GREATER THAN {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public static <T> GreaterThanCriteria<T> greaterThan( String aKey, T aValue ) {
		return new GreaterThanCriteria<>( aKey, aValue );
	}

	/**
	 * Creates an INTERSECT {@link org.refcodes.criteria.CriteriaNode}
	 * containing the provided {@link org.refcodes.criteria.Criteria} children.
	 *
	 * @param aChildren The {@link org.refcodes.criteria.Criteria} children to
	 *        be contained in the INTERSECT
	 *        {@link org.refcodes.criteria.CriteriaNode}.
	 * 
	 * @return The INTERSECT {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public static IntersectWithCriteria intersectWith( Criteria... aChildren ) {
		return new IntersectWithCriteria( aChildren );
	}

	/**
	 * Creates an LESS OR EQUAL THAN ("&lt;=")
	 * {@link org.refcodes.criteria.CriteriaLeaf} expression relating to the
	 * given key and the given reference value.
	 *
	 * @param <T> The type of the {@link org.refcodes.criteria.CriteriaLeaf}'s
	 *        value.
	 * @param aKey The key on which the reference value is to be applied.
	 * @param aValue The reference value to be applied on the key.
	 * 
	 * @return The LESS OR EQUAL THAN
	 *         {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public static <T> LessOrEqualThanCriteria<T> lessOrEqualThan( String aKey, T aValue ) {
		return new LessOrEqualThanCriteria<>( aKey, aValue );
	}

	/**
	 * Creates an LESS THAN {@link org.refcodes.criteria.CriteriaLeaf}
	 * expression relating to the given key and the given reference value.
	 *
	 * @param <T> The type of the {@link org.refcodes.criteria.CriteriaLeaf}'s
	 *        value.
	 * @param aKey The key on which the reference value is to be applied.
	 * @param aValue The reference value to be applied on the key.
	 * 
	 * @return The LESS THAN {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public static <T> LessThanCriteria<T> lessThan( String aKey, T aValue ) {
		return new LessThanCriteria<>( aKey, aValue );
	}

	/**
	 * Creates a NOT {@link org.refcodes.criteria.CriteriaNode} containing the
	 * provided {@link org.refcodes.criteria.Criteria} child.
	 *
	 * @param aCriteria The {@link org.refcodes.criteria.Criteria} child to be
	 *        contained in the NOT {@link org.refcodes.criteria.CriteriaNode}.
	 * 
	 * @return The NOT {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public static NotCriteria not( Criteria aCriteria ) {
		return new NotCriteria( aCriteria );
	}

	/**
	 * Creates an OR {@link org.refcodes.criteria.CriteriaNode} containing the
	 * provided {@link org.refcodes.criteria.Criteria} children.
	 *
	 * @param aChildren The {@link org.refcodes.criteria.Criteria} children to
	 *        be contained in the OR {@link org.refcodes.criteria.CriteriaNode}.
	 * 
	 * @return The OR {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public static OrCriteria or( Criteria... aChildren ) {
		return new OrCriteria( aChildren );
	}

	/**
	 * Creates a NOT EQUAL WITH ("&lt;&gt;")
	 * {@link org.refcodes.criteria.CriteriaLeaf} expression relating to the
	 * given key and the given reference value.
	 *
	 * @param <T> The type of the {@link org.refcodes.criteria.CriteriaLeaf}'s
	 *        value.
	 * @param aKey The key on which the reference value is to be applied.
	 * @param aValue The reference value to be applied on the key.
	 * 
	 * @return The NOT EQUAL WITH {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public static <T> NotEqualWithCriteria<T> notEqualWith( String aKey, T aValue ) {
		return new NotEqualWithCriteria<>( aKey, aValue );
	}
}
