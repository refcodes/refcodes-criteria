package org.refcodes.criteria;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The Class CriteriaUtility.
 */
public final class CriteriaUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private CriteriaUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Tries to determine the partitions specified by the given key and type
	 * being addressed by the query represented by the given criteria. Actually
	 * {@link EqualWithCriteria} values for the partition key and type (compared
	 * to the partition value) are determined embedded in
	 * {@link org.refcodes.criteria.CriteriaNode}s.
	 * -------------------------------------------------------------------------
	 * Caution: In order for partitioning to work, the provided key specifying
	 * the partition must be reflected in the criteria's key. The following
	 * rules are required in order to determine the partition from the criteria:
	 * The provided criteria must be of type
	 * {@link org.refcodes.criteria.CriteriaNode} or {@link EqualWithCriteria}.
	 * The {@link org.refcodes.criteria.CriteriaNode} must contain one ore more
	 * {@link EqualWithCriteria} referring the provided key and partition.
	 * -------------------------------------------------------------------------
	 *
	 * @param <P> the generic type
	 * @param aCriteria The criteria from which to retrieve the according
	 *        partition.
	 * @param aKey The key representing the partition criteria's key.
	 * @param aType The type representing the partition criteria's value's type.
	 * 
	 * @return A set with the values for the given key and type addressing
	 *         partition(s) to which the {@link org.refcodes.criteria.Criteria}
	 *         are targeted at.
	 * 
	 * @throws BadCriteriaException thrown in case of problems related to some
	 *         {@link Criteria}.
	 */
	public static <P> Set<P> getPartitions( Criteria aCriteria, String aKey, Class<P> aType ) throws BadCriteriaException {
		final Set<P> thePartitions = new HashSet<>();
		final HashSet<Criteria> theVistedCriteria = new HashSet<>();
		getPartitions( thePartitions, aCriteria, aKey, aType, false, theVistedCriteria );
		theVistedCriteria.clear();
		return thePartitions;
	}

	/**
	 * Recursion safe visitor for crawling the criteria.
	 *
	 * @param <C> the generic type
	 * @param aPartitions A set with the values for the given column addressing
	 *        partition(s) to which the {@link Criteria} are targeted at.
	 * @param aCriteria The criteria from which to retrieve the according
	 *        partition.
	 * @param aKey The key representing the partition criteria's key.
	 * @param aType The type representing the partition criteria's value's type
	 *        .
	 * @param isNotBranch true in case we are inside a {@link NotCriteria}
	 *        branch
	 * @param aVistedCriterias The set containing the criteria already visited
	 *        in order to prevent endless looping in case a parent
	 *        {@link Criteria} is used again in some of it's child
	 *        {@link Criteria}s.
	 * 
	 * @return the partitions
	 * 
	 * @throws BadCriteriaException in case the query is not applicable for the
	 *         partitions.
	 */
	@SuppressWarnings("unchecked")
	private static <C> void getPartitions( Set<C> aPartitions, Criteria aCriteria, String aKey, Class<C> aType, boolean isNotBranch, Set<Criteria> aVistedCriterias ) throws BadCriteriaException {
		if ( !aVistedCriterias.contains( aCriteria ) ) {
			aVistedCriterias.add( aCriteria );
			if ( aCriteria instanceof CriteriaNode theCriteriaNode ) {
				isNotBranch = isNotBranch | ( theCriteriaNode instanceof NotCriteria );
				final List<Criteria> theChildCriterias = theCriteriaNode.getChildren();
				for ( Criteria eChildCriteria : theChildCriterias ) {
					getPartitions( aPartitions, eChildCriteria, aKey, aType, isNotBranch, aVistedCriterias );
				}
			}
			else if ( aCriteria instanceof CriteriaLeaf<?> ) {
				final CriteriaLeaf<?> theCriteriaLeaf = (CriteriaLeaf<?>) aCriteria;
				if ( theCriteriaLeaf.getKey() != null && theCriteriaLeaf.getValue() != null ) {
					if ( theCriteriaLeaf.getKey().equals( aKey ) && aType.isAssignableFrom( theCriteriaLeaf.getValue().getClass() ) ) {
						if ( isNotBranch || !( theCriteriaLeaf instanceof EqualWithCriteria ) ) {
							throw new ComplexCriteriaException( aCriteria, "The provided criteria is too complex in order to determin the set of relevant partitions! Either the partition key \"" + aKey + "\" with type <" + aType.getName() + "> is contained inside a NOT node's child or included in a criteria different from an EQUALS criteria leaf!" );
						}
						else {
							aPartitions.add( (C) theCriteriaLeaf.getValue() );
						}
					}
				}
			}
		}
	}

	/**
	 * Removes the criteria for the provided partitions given key. Make sure
	 * that the criteria does not address more than one(!) partition in case you
	 * use this method with a e.g. <code>PartedLoggerImpl</code>. Only in case
	 * one partition is addressed by the criteria, then the partition to be
	 * addressed is known and then we can safely remove the criteria. Use the
	 * {@link #getPartitions(Criteria, String, Class)} method for getting the
	 * partitions being addressed by the given criteria. When there is just one
	 * partition being addressed, then this partition's criteria may be removed
	 * from the criteria: The criteria's query is targeted at the according
	 * partition, we do not need to include the select statement for that
	 * partition! Actually {@link EqualWithCriteria} for the partition columns
	 * and the given partition are removed.
	 * -------------------------------------------------------------------------
	 * Caution: In order for partitioning to work, the provided key specifying
	 * the partition must be reflected in the criteria's key. The following
	 * rules are required in order to determine the partition from the criteria:
	 * The provided criteria must be of type
	 * {@link org.refcodes.criteria.CriteriaNode} or {@link EqualWithCriteria}.
	 * The {@link org.refcodes.criteria.CriteriaNode} must contain one ore more
	 * {@link EqualWithCriteria} referring the provided key and partition.
	 * -------------------------------------------------------------------------
	 *
	 * @param <C> the generic type
	 * @param aCriteria The criteria from which to retrieve the according
	 *        partition
	 * @param aKey The key representing the partition criteria's key.
	 * @param aPartitions The partitions for which to remove the criteria
	 * 
	 * @return The criteria without the criteria referring to the provided key
	 *         and the provided partitions
	 */
	public static <C> Criteria doRemovePartitionCriteria( Criteria aCriteria, String aKey, Set<C> aPartitions ) {
		if ( aPartitions == null || aPartitions.isEmpty() ) {
			return aCriteria;
		}
		final HashSet<Criteria> theVistedCriteria = new HashSet<>();
		final Criteria theCriteria = doRemovePartitionCriteria( aCriteria, aKey, aPartitions, theVistedCriteria );
		theVistedCriteria.clear();
		return theCriteria;
	}

	/**
	 * Recursion safe visitor for crawling the criteria.
	 *
	 * @param <C> the generic type
	 * @param aCriteria The criteria from which to retrieve the according
	 *        partition
	 * @param aKey The key representing the partition criteria's key.
	 * @param aPartitions The partitions for which to remove the criteria
	 * @param aVistedCriterias The set containing the criteria already visited
	 *        in order to prevent endless looping in case a parent
	 *        {@link Criteria} is used again in some of it's child
	 *        {@link Criteria}s.
	 * 
	 * @return The criteria without the criteria referring to the provided key
	 *         and the provided partitions
	 */
	private static <C> Criteria doRemovePartitionCriteria( Criteria aCriteria, String aKey, Set<C> aPartitions, Set<Criteria> aVistedCriterias ) {
		if ( aVistedCriterias.contains( aCriteria ) ) {
			return null;
		}
		aVistedCriterias.add( aCriteria );
		// Criteria node:
		if ( aCriteria instanceof CriteriaNode ) {
			CriteriaLeaf<?> eCriteriaLeaf;
			final CriteriaNode theCriteriaNode = (CriteriaNode) aCriteria;
			Criteria eResultCriteria;
			final List<Criteria> theChildCriterias = new ArrayList<>( theCriteriaNode.getChildren() );
			int eIndex;
			for ( Criteria eChildCriteria : theChildCriterias ) {
				// Criteria item:
				if ( eChildCriteria instanceof CriteriaLeaf<?> ) {
					eCriteriaLeaf = (CriteriaLeaf<?>) eChildCriteria;
					if ( eCriteriaLeaf.getKey() != null && eCriteriaLeaf.getValue() != null ) {
						// @formatter:off
						if ( eCriteriaLeaf.getKey().equals( aKey ) /* && aPartitions.iterator().next().getClass().isAssignableFrom(eCriteriaLeaf.getValue().getClass() ) */&& aPartitions.contains( eCriteriaLeaf.getValue() ) ) {
							theCriteriaNode.getChildren().remove( eCriteriaLeaf );
						}
						// @formatter:on
					}
				}
				// Criteria node:
				if ( eChildCriteria instanceof CriteriaNode ) {
					eResultCriteria = doRemovePartitionCriteria( eChildCriteria, aKey, aPartitions, aVistedCriterias );
					if ( eResultCriteria == null ) {
						theCriteriaNode.getChildren().remove( eChildCriteria );
					}
					else if ( eResultCriteria != eChildCriteria ) {
						eIndex = theCriteriaNode.getChildren().indexOf( eChildCriteria );
						if ( eIndex != -1 ) {
							theCriteriaNode.getChildren().remove( eIndex );
							theCriteriaNode.getChildren().add( eIndex, eResultCriteria );
						}
					}
				}
			}
			if ( theCriteriaNode.getChildren().size() == 1 ) {
				return theCriteriaNode.getChildren().get( 0 );
			}
			else if ( theCriteriaNode.getChildren().size() == 0 ) {
				return null;
			}
		}
		// Criteria item:
		else if ( aCriteria instanceof CriteriaLeaf<?> ) {
			final CriteriaLeaf<?> theCriteriaLeaf = (CriteriaLeaf<?>) aCriteria;
			if ( theCriteriaLeaf.getKey() != null && theCriteriaLeaf.getValue() != null ) {
				// @formatter:off
				if ( theCriteriaLeaf.getKey().equals( aKey ) /* && aPartitions.iterator().next().getClass().isAssignableFrom(eCriteriaLeaf.getValue().getClass() ) */&& aPartitions.contains( theCriteriaLeaf.getValue() ) ) {
					return null;
				}
				// @formatter:on
			}
		}
		return aCriteria;
	}
}
