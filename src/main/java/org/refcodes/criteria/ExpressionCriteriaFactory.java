// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import static org.refcodes.criteria.CriteriaSugar.*;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.matheclipse.parser.client.Parser;
import org.matheclipse.parser.client.SyntaxError;
import org.matheclipse.parser.client.ast.ASTNode;
import org.matheclipse.parser.client.ast.FunctionNode;
import org.matheclipse.parser.client.ast.NumberNode;
import org.matheclipse.parser.client.ast.SymbolNode;
import org.refcodes.data.CharSet;
import org.refcodes.data.QuotationMark;
import org.refcodes.data.Wildcard;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;
import org.refcodes.textual.EscapeTextBuilder;
import org.refcodes.textual.EscapeTextMode;
import org.refcodes.textual.ReplaceTextBuilder;

/**
 * Implements a {@link org.refcodes.criteria.CriteriaFactory} which is capable
 * of parsing an expression such as the following one: ( ( ( City = 'Berlin' )
 * OR ( City = 'Munich' ) ) AND ( Surname = 'Miller' ) ).
 */
public class ExpressionCriteriaFactory implements CriteriaFactory<String> {

	private static final Pattern PATTERN = Pattern.compile( "(?:^|\\s)" + QuotationMark.SINGLE_QUOTE.getChar() + "([^" + QuotationMark.SINGLE_QUOTE.getChar() + "]*?)" + QuotationMark.SINGLE_QUOTE.getChar() + "(?:$|\\s)", Pattern.MULTILINE );
	private static final String EQUAL_WITH = "Set";
	private static final String NOT_EQUAL_WITH = "Unequal";
	private static final String LESS_OR_EQUAL_THAN = "LessEqual";
	private static final String LESS_THAN = "Less";
	private static final String GREATER_OR_EQUAL_THAN = "GreaterEqual";
	private static final String GREATER_THAN = "Greater";
	private static final String AND = "AND";
	private static final String OR = "OR";
	private static final String NOT = "NOT";
	private static final String INTERSECT = "INTERSECTION";
	private static final String NODE = "Times";
	private static final String SINGLE_QUOTE_TOKEN = "XXXSINGLEQUOTEXXX";

	/** We assume that the below strings are never used in a regular query:. */
	private static final Property[] ESCAPE_SPECIAL_CHARS = new Property[] { new PropertyImpl( " ", "XXXSPACEXXX" ), new PropertyImpl( "-", "XXXMINUSXXX" ), new PropertyImpl( "+", "XXXPLUSXXX" ), new PropertyImpl( "*", "XXXASTERISKXXX" ), new PropertyImpl( "/", "XXXSLASHXXX" ), new PropertyImpl( "~", "XXXTILDEXXX" ), new PropertyImpl( "#", "XXXHASHXXX" ), new PropertyImpl( "_", "XXXUNDERSCOREXXX" ), new PropertyImpl( "=", "XXXEQUALXXX" ), new PropertyImpl( ">", "XXXGREATERXXX" ), new PropertyImpl( "<", "XXXLESSXXX" ), new PropertyImpl( "'", SINGLE_QUOTE_TOKEN )

	};

	private static final Property[] ESCAPE_NUMBERS = new Property[] { new PropertyImpl( "1", "XXX1XXX" ), new PropertyImpl( "2", "XXX2XXX" ), new PropertyImpl( "3", "XXX3XXX" ), new PropertyImpl( "4", "XXX4XXX" ), new PropertyImpl( "5", "XXX5XXX" ), new PropertyImpl( "6", "XXX6XXX" ), new PropertyImpl( "7", "XXX7XXX" ), new PropertyImpl( "8", "XXX8XXX" ), new PropertyImpl( "9", "XXX9XXX" ), new PropertyImpl( "0", "XXX0XXX" ), new PropertyImpl( ".", "XXXDOTXXX" ), new PropertyImpl( ",", "XXXCOMMAXXX" )

	};

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Criteria fromQuery( String aQuery ) throws ParseException {

		// 1.: Normalize the query to use a simple regular expression:
		aQuery = toNormalized( aQuery );
		// 2.: Now escape chars being reserved by the used parser:
		aQuery = toEscaped( aQuery );
		// 3.: Now remove any remaining single quotes:
		// aQuery = toStripped( aQuery );

		if ( Wildcard.QUERY.getValue().equals( aQuery ) ) {
			return null;
		}

		final Parser theParser = new Parser();
		try {
			final ASTNode theNode = theParser.parse( aQuery );
			if ( theNode instanceof FunctionNode ) {
				return toCriteria( (FunctionNode) theNode );

			}
		}
		catch ( SyntaxError e ) {
			throw new ParseException( aQuery, e.getStartOffset() );
		}
		throw new ParseException( aQuery, 0 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Internal helper using the mathclipse framework.
	 * 
	 * @param aFunctionNode The node to be processed.
	 * 
	 * @return The {@link Criteria} (tree) extracted from the provided node.
	 * 
	 * @throws ParseException Thrown in case there were problems working with
	 *         the given node.
	 */
	private Criteria toCriteria( FunctionNode aFunctionNode ) throws ParseException {
		if ( aFunctionNode.size() == 3 ) {
			if ( ( aFunctionNode.get( 0 ) instanceof SymbolNode ) && ( aFunctionNode.get( 1 ) instanceof SymbolNode ) && ( aFunctionNode.get( 2 ) instanceof SymbolNode ) ) {
				return toCriteria( (SymbolNode) aFunctionNode.get( 0 ), (SymbolNode) aFunctionNode.get( 1 ), aFunctionNode.get( 2 ) );
			}
			else if ( ( aFunctionNode.get( 0 ) instanceof SymbolNode ) && ( aFunctionNode.get( 1 ) instanceof SymbolNode ) && ( aFunctionNode.get( 2 ) instanceof NumberNode ) ) {
				return toCriteria( (SymbolNode) aFunctionNode.get( 0 ), (SymbolNode) aFunctionNode.get( 1 ), aFunctionNode.get( 2 ) );
			}
			else if ( ( aFunctionNode.get( 0 ) instanceof SymbolNode ) && ( aFunctionNode.get( 1 ) instanceof FunctionNode ) && ( aFunctionNode.get( 2 ) instanceof SymbolNode ) ) {
				return toCriteria( (SymbolNode) aFunctionNode.get( 0 ), (FunctionNode) aFunctionNode.get( 1 ), (SymbolNode) aFunctionNode.get( 2 ) );
			}
			else if ( ( aFunctionNode.get( 0 ) instanceof SymbolNode ) && ( aFunctionNode.get( 1 ) instanceof FunctionNode ) && ( aFunctionNode.get( 2 ) instanceof FunctionNode ) ) {
				return toCriteria( (SymbolNode) aFunctionNode.get( 0 ), (FunctionNode) aFunctionNode.get( 1 ), (FunctionNode) aFunctionNode.get( 2 ) );
			}
			else if ( ( aFunctionNode.get( 0 ) instanceof SymbolNode ) && ( aFunctionNode.get( 1 ) instanceof SymbolNode ) && ( aFunctionNode.get( 2 ) instanceof FunctionNode ) ) {
				return toCriteria( (SymbolNode) aFunctionNode.get( 0 ), (SymbolNode) aFunctionNode.get( 1 ), (FunctionNode) aFunctionNode.get( 2 ) );
			}
		}
		throw new ParseException( aFunctionNode.getString(), 0 );
	}

	/**
	 * Internal helper using the mathclipse framework.
	 * 
	 * @param aSymbolNodeA The node A to be processed.
	 * @param aSymbolNodeB The node B to be processed.
	 * @param aSymbolNodeC The node C to be processed.
	 * 
	 * @return The {@link Criteria} (tree) extracted from the provided nodes.
	 * 
	 * @throws ParseException Thrown in case there were problems working with
	 *         the given node.
	 */
	private Criteria toCriteria( SymbolNode aSymbolNodeA, SymbolNode aSymbolNodeB, ASTNode aSymbolNodeC ) throws ParseException {

		final String theUnEscapedKey = toUnEscapedKey( aSymbolNodeB.getString() );
		final Object theUnEscapedValue = toUnEscapedValue( aSymbolNodeC.getString() );

		if ( aSymbolNodeA.getString().equalsIgnoreCase( EQUAL_WITH ) ) {
			return equalWith( theUnEscapedKey, theUnEscapedValue );
		}
		else if ( aSymbolNodeA.getString().equalsIgnoreCase( NOT_EQUAL_WITH ) ) {
			return notEqualWith( theUnEscapedKey, theUnEscapedValue );
		}
		else if ( aSymbolNodeA.getString().equalsIgnoreCase( GREATER_THAN ) ) {
			return greaterThan( theUnEscapedKey, theUnEscapedValue );
		}
		else if ( aSymbolNodeA.getString().equalsIgnoreCase( GREATER_OR_EQUAL_THAN ) ) {
			return greaterOrEqualThan( theUnEscapedKey, theUnEscapedValue );
		}
		else if ( aSymbolNodeA.getString().equalsIgnoreCase( LESS_THAN ) ) {
			return lessThan( theUnEscapedKey, theUnEscapedValue );
		}
		else if ( aSymbolNodeA.getString().equalsIgnoreCase( LESS_OR_EQUAL_THAN ) ) {
			return lessOrEqualThan( theUnEscapedKey, theUnEscapedValue );
		}
		throw new ParseException( toUnEscapedKey( "Unable to parse: " + aSymbolNodeA.getString() ) + "\", \"" + theUnEscapedKey + "\" and/or \"" + theUnEscapedValue + "\"", 0 );
	}

	/**
	 * Internal helper using the mathclipse framework.
	 * 
	 * @param aSymbolNodeA The node A to be processed.
	 * @param aFunctionNodeB The node B to be processed.
	 * @param aSymbolNodeC The node C to be processed.
	 * 
	 * @return The {@link Criteria} (tree) extracted from the provided nodes.
	 * 
	 * @throws ParseException Thrown in case there were problems working with
	 *         the given node.
	 */
	private Criteria toCriteria( SymbolNode aSymbolNodeA, FunctionNode aFunctionNodeB, SymbolNode aSymbolNodeC ) throws ParseException {
		if ( aSymbolNodeA.getString().equalsIgnoreCase( NODE ) ) {
			final Criteria theCriteria = toCriteria( aFunctionNodeB );
			if ( aSymbolNodeC.getString().equalsIgnoreCase( OR ) ) {
				return or( theCriteria );
			}
			else if ( aSymbolNodeC.getString().equalsIgnoreCase( AND ) ) {
				return and( theCriteria );
			}
			else if ( aSymbolNodeC.getString().equalsIgnoreCase( INTERSECT ) ) {
				return intersectWith( theCriteria );
			}
			else if ( aSymbolNodeC.getString().equalsIgnoreCase( NOT ) ) {
				return not( theCriteria );
			}
		}
		throw new ParseException( "Unable to parse: \"" + aSymbolNodeA.getString() + "\", \"" + aFunctionNodeB.getString() + "\" and/or \"" + aSymbolNodeC.getString() + "\"", 0 );
	}

	/**
	 * Internal helper using the mathclipse framework.
	 * 
	 * @param aSymbolNodeA The node A to be processed.
	 * @param aSymbolNodeB The node B to be processed.
	 * @param aFunctionNodeC The node C to be processed.
	 * 
	 * @return The {@link Criteria} (tree) extracted from the provided nodes.
	 * 
	 * @throws ParseException Thrown in case there were problems working with
	 *         the given node.
	 */
	private Criteria toCriteria( SymbolNode aSymbolNodeA, SymbolNode aSymbolNodeB, FunctionNode aFunctionNodeC ) throws ParseException {
		if ( aSymbolNodeA.getString().equalsIgnoreCase( NODE ) ) {
			final Criteria theCriteria = toCriteria( aFunctionNodeC );
			if ( aSymbolNodeB.getString().equalsIgnoreCase( NOT ) ) {
				return not( theCriteria );
			}
		}
		throw new ParseException( "Unable to parse: \"" + aSymbolNodeA.getString() + "\", \"" + aSymbolNodeB.getString() + "\" and/or \"" + aFunctionNodeC.getString() + "\"", 0 );
	}

	/**
	 * Internal helper using the mathclipse framework.
	 * 
	 * @param aSymbolNodeA The node A to be processed.
	 * @param aFunctionNodeB The node B to be processed.
	 * @param aFunctionNodeC The node C to be processed.
	 * 
	 * @return The {@link Criteria} (tree) extracted from the provided nodes.
	 * 
	 * @throws ParseException Thrown in case there were problems working with
	 *         the given node.
	 */
	private Criteria toCriteria( SymbolNode aSymbolNodeA, FunctionNode aFunctionNodeB, FunctionNode aFunctionNodeC ) throws ParseException {
		if ( aSymbolNodeA.getString().equalsIgnoreCase( NODE ) ) {
			final Criteria theCriteriaB = toCriteria( aFunctionNodeB );
			final Criteria theCriteriaC = toCriteria( aFunctionNodeC );
			if ( theCriteriaB instanceof CriteriaNode theCriteriaNode ) {
				theCriteriaNode.addChild( theCriteriaC );
				return theCriteriaNode;
			}
		}
		throw new ParseException( "Unable to parse: \"" + aSymbolNodeA.getString() + "\", \"" + aFunctionNodeB.getString() + "\" and/or \"" + aFunctionNodeC.getString() + "\"", 0 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Normalized the allowed single quotes to be the default single quote.
	 * 
	 * @param aText The text to be normalized.
	 * 
	 * @return The normalized text.
	 */
	private String toNormalized( String aText ) {
		for ( int i = 0; i < CharSet.QUOTES.getCharSet().length; i++ ) {
			aText = aText.replaceAll( "" + CharSet.QUOTES.getCharSet()[i], "" + QuotationMark.SINGLE_QUOTE.getChar() );
		}
		return aText;
	}

	/**
	 * Escapes the given text.
	 * 
	 * @param aText The text to escape.
	 * 
	 * @return The escaped text.
	 */
	private String toEscaped( String aText ) {
		final Matcher theMatcher = PATTERN.matcher( aText );
		String eSubText;
		String eEscapedSubText;
		while ( theMatcher.find() ) {
			eSubText = theMatcher.group( 1 );
			eEscapedSubText = eSubText;
			eEscapedSubText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.ESCAPE ).withText( eEscapedSubText ).withEscapeProperties( ESCAPE_SPECIAL_CHARS ).toString();
			aText = new ReplaceTextBuilder().withText( aText ).withFindText( QuotationMark.SINGLE_QUOTE.getChar() + eSubText + QuotationMark.SINGLE_QUOTE.getChar() ).withReplaceText( SINGLE_QUOTE_TOKEN + eEscapedSubText + SINGLE_QUOTE_TOKEN ).toString();
		}
		aText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.ESCAPE ).withText( aText ).withEscapeProperties( ESCAPE_NUMBERS ).toString();
		return aText;
	}

	/**
	 * Strips the given text by removing the default single quote char.
	 * 
	 * @param aText The text to be stripped.
	 * 
	 * @return The stripped text.
	 */
	// @formatter:off
	//	private String toStripped( String aText ) {
	//		return aText.replaceAll( "" + RefcodesConstants.DEFAULT_SINGLE_QUOTE, "" );
	//	}
	// @formatter:on

	/**
	 * Unescapes the given text.
	 * 
	 * @param aText The text to unescape.
	 * 
	 * @return The unescaped text.
	 */
	private String toUnEscapedKey( String aText ) {
		aText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.UNESCAPE ).withText( aText ).withEscapeProperties( ESCAPE_NUMBERS ).toString();
		aText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.UNESCAPE ).withText( aText ).withEscapeProperties( ESCAPE_SPECIAL_CHARS ).toString();
		return aText;
	}

	/**
	 * Unescapes the given text.
	 * 
	 * @param aText The text to unescape.
	 * 
	 * @return The unescaped text.
	 */
	private Object toUnEscapedValue( String aText ) {
		aText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.UNESCAPE ).withText( aText ).withEscapeProperties( ESCAPE_NUMBERS ).toString();
		aText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.UNESCAPE ).withText( aText ).withEscapeProperties( ESCAPE_SPECIAL_CHARS ).toString();
		try {
			final Long theLong = Long.valueOf( aText );
			return theLong;
		}
		catch ( NumberFormatException e1 ) {
			try {
				final Double theDouble = Double.valueOf( aText );
				return theDouble;
			}
			catch ( NumberFormatException e2 ) {
				if ( aText.startsWith( "" + QuotationMark.SINGLE_QUOTE.getChar() ) ) {
					aText = aText.substring( 1 );
				}
				if ( aText.endsWith( "" + QuotationMark.SINGLE_QUOTE.getChar() ) ) {
					aText = aText.substring( 0, aText.length() - 1 );
				}
				return aText;
			}
		}
	}
}
