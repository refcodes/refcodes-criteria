package org.refcodes.criteria;

import java.util.Iterator;

import org.refcodes.data.Literal;
import org.refcodes.data.QuotationMark;

/**
 * The {@link org.refcodes.criteria.ExpressionCriteriaFactory} is capable of
 * creating query {@link java.lang.String} instances from
 * {@link org.refcodes.criteria.Criteria} instances; the query
 * {@link java.lang.String} instances which can be used as parts of SQL
 * statements.
 */
public class ExpressionQueryFactory implements QueryFactory<String> {

	private static final String IS = " IS ";

	private static final String EQUAL_WITH = " = ";

	private static final String IS_NOT = " IS NOT ";

	private static final String NOT_EQUAL_WITH = " != ";

	private static final String GREATER_THAN = " > ";

	private static final String GREATER_OR_EQUAL_THAN = " >= ";

	private static final String LESS_THAN = " < ";

	private static final String LESS_OR_EQUAL_THAN = " <= ";

	private static final String AND = " AND ";

	private static final String OR = " OR ";

	private static final String NOT = "NOT ";

	private static final String INTERSECTION = " INTERSECTION ";

	private static final String SINGLE_QUOTE = "" + QuotationMark.SINGLE_QUOTE.getChar();

	private static final String DELIMITER = ", ";

	private static final String BRACE_OPEN = "( ";

	private static final String BRACE_CLOSE = " )";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String fromCriteria( Criteria aCriteria ) {
		return toQuery( aCriteria, true );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Generates a query string for the given {@link CriteriaNode}.
	 * 
	 * @param aCriteriaNode The {@link CriteriaNode} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaNode} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaNode} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaNode}.
	 */
	private String toQuery( NotCriteria aCriteriaNode, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		theQueryBuffer.append( NOT );
		final Iterator<Criteria> e = aCriteriaNode.getChildren().iterator();
		while ( e.hasNext() ) {
			theQueryBuffer.append( toQuery( e.next(), false ) );
			if ( e.hasNext() ) {
				theQueryBuffer.append( DELIMITER );
			}
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given {@link CriteriaNode}.
	 * 
	 * @param aCriteriaNode The {@link CriteriaNode} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaNode} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaNode} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaNode}.
	 */
	private String toQuery( AndCriteria aCriteriaNode, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		final Iterator<Criteria> e = aCriteriaNode.getChildren().iterator();
		while ( e.hasNext() ) {
			theQueryBuffer.append( toQuery( e.next(), false ) );
			if ( e.hasNext() ) {
				theQueryBuffer.append( AND );
			}
		}
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given {@link CriteriaNode}.
	 * 
	 * @param aCriteriaNode The {@link CriteriaNode} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaNode} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaNode} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaNode}.
	 */
	private String toQuery( OrCriteria aCriteriaNode, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		final Iterator<Criteria> e = aCriteriaNode.getChildren().iterator();
		while ( e.hasNext() ) {
			theQueryBuffer.append( toQuery( e.next(), false ) );
			if ( e.hasNext() ) {
				theQueryBuffer.append( OR );
			}
		}
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given
	 * {@link org.refcodes.criteria.CriteriaNode}.
	 *
	 * @param aCriteriaNode The {@link org.refcodes.criteria.CriteriaNode} for
	 *        which to generate the query.
	 * @param isCriteriaTreeRoot True in case the
	 *        {@link org.refcodes.criteria.CriteriaNode} is the root of the
	 *        {@link org.refcodes.criteria.Criteria} tree. Being root means
	 *        dedicated handling of the
	 *        {@link org.refcodes.criteria.CriteriaNode} then not being root.
	 * 
	 * @return The query generated from the given
	 *         {@link org.refcodes.criteria.CriteriaNode}.
	 */
	public String toQuery( IntersectWithCriteria aCriteriaNode, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		final Iterator<Criteria> e = aCriteriaNode.getChildren().iterator();
		while ( e.hasNext() ) {
			theQueryBuffer.append( toQuery( e.next(), false ) );
			if ( e.hasNext() ) {
				theQueryBuffer.append( INTERSECTION );
			}
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given {@link CriteriaLeaf}.
	 * 
	 * @param aCriteriaLeaf The {@link CriteriaLeaf} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaLeaf} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaLeaf} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaLeaf}.
	 */
	private String toQuery( EqualWithCriteria<?> aCriteriaLeaf, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		theQueryBuffer.append( aCriteriaLeaf.getKey() );
		if ( Literal.NULL.getValue().equalsIgnoreCase( aCriteriaLeaf.getValue().toString() ) ) {
			theQueryBuffer.append( IS );
		}
		else {
			theQueryBuffer.append( EQUAL_WITH );
		}
		appendCriteriaLeaf( aCriteriaLeaf, theQueryBuffer );
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given {@link CriteriaLeaf}.
	 * 
	 * @param aCriteriaLeaf The {@link CriteriaLeaf} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaLeaf} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaLeaf} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaLeaf}.
	 */
	private String toQuery( NotEqualWithCriteria<?> aCriteriaLeaf, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		theQueryBuffer.append( aCriteriaLeaf.getKey() );
		if ( isNullCriteriaLeaf( aCriteriaLeaf ) ) {
			theQueryBuffer.append( IS_NOT );
		}
		else {
			theQueryBuffer.append( NOT_EQUAL_WITH );
		}
		appendCriteriaLeaf( aCriteriaLeaf, theQueryBuffer );
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given {@link CriteriaLeaf}.
	 * 
	 * @param aCriteriaLeaf The {@link CriteriaLeaf} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaLeaf} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaLeaf} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaLeaf}.
	 */
	private String toQuery( GreaterThanCriteria<?> aCriteriaLeaf, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		theQueryBuffer.append( aCriteriaLeaf.getKey() );
		theQueryBuffer.append( GREATER_THAN );
		appendCriteriaLeaf( aCriteriaLeaf, theQueryBuffer );
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given {@link CriteriaLeaf}.
	 * 
	 * @param aCriteriaLeaf The {@link CriteriaLeaf} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaLeaf} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaLeaf} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaLeaf}.
	 */
	private String toQuery( GreaterOrEqualThanCriteria<?> aCriteriaLeaf, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		theQueryBuffer.append( aCriteriaLeaf.getKey() );
		theQueryBuffer.append( GREATER_OR_EQUAL_THAN );
		appendCriteriaLeaf( aCriteriaLeaf, theQueryBuffer );
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given
	 * {@link org.refcodes.criteria.CriteriaLeaf}.
	 *
	 * @param aCriteriaLeaf The {@link org.refcodes.criteria.CriteriaLeaf} for
	 *        which to generate the query.
	 * @param isCriteriaTreeRoot True in case the
	 *        {@link org.refcodes.criteria.CriteriaLeaf} is the root of the
	 *        {@link org.refcodes.criteria.Criteria} tree. Being root means
	 *        dedicated handling of the
	 *        {@link org.refcodes.criteria.CriteriaLeaf} then not being root.
	 * 
	 * @return The query generated from the given
	 *         {@link org.refcodes.criteria.CriteriaLeaf}.
	 */
	public String toQuery( LessThanCriteria<?> aCriteriaLeaf, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		theQueryBuffer.append( aCriteriaLeaf.getKey() );
		theQueryBuffer.append( LESS_THAN );
		appendCriteriaLeaf( aCriteriaLeaf, theQueryBuffer );
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Generates a query string for the given {@link CriteriaLeaf}.
	 * 
	 * @param aCriteriaLeaf The {@link CriteriaLeaf} for which to generate the
	 *        query.
	 * @param isCriteriaTreeRoot True in case the {@link CriteriaLeaf} is the
	 *        root of the {@link Criteria} tree. Being root means dedicated
	 *        handling of the {@link CriteriaLeaf} then not being root.
	 * 
	 * @return The query generated from the given {@link CriteriaLeaf}.
	 */
	private String toQuery( LessOrEqualThanCriteria<?> aCriteriaLeaf, boolean isCriteriaTreeRoot ) {
		final StringBuilder theQueryBuffer = new StringBuilder();
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_OPEN );
		}
		theQueryBuffer.append( aCriteriaLeaf.getKey() );
		theQueryBuffer.append( LESS_OR_EQUAL_THAN );
		appendCriteriaLeaf( aCriteriaLeaf, theQueryBuffer );
		if ( !isCriteriaTreeRoot ) {
			theQueryBuffer.append( BRACE_CLOSE );
		}
		return theQueryBuffer.toString();
	}

	/**
	 * Switch method (why not polymorphism here: Java decides at compile time
	 * which method to call, even if the runtime type is a subclass of the
	 * declared type and there exist dedicated methods for the actual subtype)
	 * selecting the method to handle the actual criteria type.
	 *
	 * @param aCriteria The criteria to be processed.
	 * @param isCriteriaTreeRoot the is criteria tree root
	 * 
	 * @return The query generated from the query.
	 */
	private String toQuery( Criteria aCriteria, boolean isCriteriaTreeRoot ) {
		if ( aCriteria instanceof AndCriteria ) {
			return toQuery( (AndCriteria) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof OrCriteria ) {
			return toQuery( (OrCriteria) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof NotCriteria ) {
			return toQuery( (NotCriteria) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof IntersectWithCriteria ) {
			return toQuery( (IntersectWithCriteria) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof EqualWithCriteria ) {
			return toQuery( (EqualWithCriteria<?>) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof NotEqualWithCriteria ) {
			return toQuery( (NotEqualWithCriteria<?>) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof GreaterThanCriteria ) {
			return toQuery( (GreaterThanCriteria<?>) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof GreaterOrEqualThanCriteria ) {
			return toQuery( (GreaterOrEqualThanCriteria<?>) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof LessThanCriteria ) {
			return toQuery( (LessThanCriteria<?>) aCriteria, isCriteriaTreeRoot );
		}

		if ( aCriteria instanceof LessOrEqualThanCriteria ) {
			return toQuery( (LessOrEqualThanCriteria<?>) aCriteria, isCriteriaTreeRoot );
		}

		throw new UnknownCriteriaRuntimeException( "The criteria with alias \"" + aCriteria.getAlias() + "\" is unknown by this query factory.", aCriteria );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static void appendCriteriaLeaf( CriteriaLeaf<?> aCriteriaLeaf, StringBuilder aQueryBuffer ) {
		final boolean isQuote = ( !( aCriteriaLeaf.getValue() instanceof Number ) ) && ( !isNullCriteriaLeaf( aCriteriaLeaf ) );
		if ( isQuote ) {
			aQueryBuffer.append( SINGLE_QUOTE );
		}
		if ( isNullCriteriaLeaf( aCriteriaLeaf ) ) {
			aQueryBuffer.append( Literal.NULL.getValue().toUpperCase() );
		}
		else {
			aQueryBuffer.append( aCriteriaLeaf.getValue() );
		}
		if ( isQuote ) {
			aQueryBuffer.append( SINGLE_QUOTE );
		}
	}

	private static boolean isNullCriteriaLeaf( CriteriaLeaf<?> aCriteriaLeaf ) {
		return ( ( aCriteriaLeaf.getValue() instanceof String ) && ( (String) aCriteriaLeaf.getValue() ).equalsIgnoreCase( Literal.NULL.getValue() ) );
	}
}
