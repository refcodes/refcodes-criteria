// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * A {@link org.refcodes.criteria.CriteriaLeaf} representing a GREATER THAN
 * expression.
 * 
 * @param <T> the generic type of the criteria's value.
 */
public class GreaterThanCriteria<T> extends AbstractCriteriaLeaf<T> {

	public static final String ALIAS = "GREATER_THAN";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor setting the name for the {@link Criteria}.
	 */
	public GreaterThanCriteria() {
		super( ALIAS );
	}

	/**
	 * Sets the key and the according value for the {@link Criteria},.
	 *
	 * @param aKey The key for which the {@link Criteria} is responsible.
	 * @param aValue The value referenced by this {@link Criteria}.
	 */
	public GreaterThanCriteria( String aKey, T aValue ) {
		super( ALIAS, aKey, aValue );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CriteriaSchema toSchema() {
		return super.toSchema( "Logical GREATER (>) expression." );
	}
}
