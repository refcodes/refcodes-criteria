// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * A {@link org.refcodes.criteria.CriteriaNode} representing a logical INTERSECT
 * operator.
 */
public class IntersectWithCriteria extends AbstractCriteriaNode {

	public static final String ALIAS = "INTERSECTION";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor setting the name for the {@link Criteria}.
	 */
	public IntersectWithCriteria() {
		super( ALIAS );
	}

	/**
	 * Constructor setting the children {@link Criteria} of the
	 * {@link Criteria}.
	 * 
	 * @param aChildren The children {@link Criteria} instances to be set.
	 */
	public IntersectWithCriteria( Criteria... aChildren ) {
		super( ALIAS, aChildren );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CriteriaSchema toSchema() {
		return super.toSchema( "Logical INTERSECTS expression." );
	}
}
