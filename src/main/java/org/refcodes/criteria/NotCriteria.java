// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A {@link org.refcodes.criteria.CriteriaNode} representing a logical NOT
 * operator.
 */
public class NotCriteria extends AbstractCriteriaNode {

	public static final String ALIAS = "NOT";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor setting the name for the {@link Criteria}.
	 */
	public NotCriteria() {
		super( ALIAS, Collections.unmodifiableList( new ArrayList<Criteria>() ) );
	}

	/**
	 * Constructor setting the child {@link Criteria} for the {@link Criteria}.
	 * 
	 * @param aChild The child {@link Criteria} instance to be set.
	 */
	public NotCriteria( Criteria aChild ) {
		super( ALIAS, List.of( aChild ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addChild( Criteria aChild ) {
		if ( getChildren().isEmpty() ) {
			setChild( aChild );
		}
		else {
			throw new IllegalStateException( "This instance must not contain any criteria in order to add a child, though it already contains <" + getChildren().size() + "> elements!" );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CriteriaSchema toSchema() {
		return super.toSchema( "Logical NOT (!) expression." );
	}

	/**
	 * Sets the child {@link Criteria} for the {@link Criteria}.
	 * 
	 * @param aChild The child {@link Criteria} instance to be set.
	 */
	public void setChild( Criteria aChild ) {
		_children = List.of( aChild );
	}
}
