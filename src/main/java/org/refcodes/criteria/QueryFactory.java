// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * The {@link org.refcodes.criteria.QueryFactory} generates a query from the
 * provided {@link org.refcodes.criteria.Criteria} (tree). The resulting query
 * may be targeted at a database and therefore be SQL like. CAUTION: The syntax
 * supported for the query statement is implementation depended!
 *
 * @author steiner
 * 
 * @version $Id: $Id
 * 
 * @param <Q> The type of the query statement, usually a
 *        {@link java.lang.String}.
 */
public interface QueryFactory<Q> {

	/**
	 * Generates a query from the provided
	 * {@link org.refcodes.criteria.Criteria} (tree).
	 *
	 * @param aCriteria The {@link org.refcodes.criteria.Criteria} from which to
	 *        generate the query.
	 * 
	 * @return A query generated from the provided
	 *         {@link org.refcodes.criteria.Criteria} (tree).
	 */
	Q fromCriteria( Criteria aCriteria );
}
