// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import org.refcodes.criteria.CriteriaRuntimeException.CriteriaCriteriaRuntimeException;

/**
 * The Class {@link UnknownCriteriaRuntimeException}.
 */
public class UnknownCriteriaRuntimeException extends CriteriaCriteriaRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public UnknownCriteriaRuntimeException( String aMessage, Criteria aCriteria, String aErrorCode ) {
		super( aMessage, aCriteria, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownCriteriaRuntimeException( String aMessage, Criteria aCriteria, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCriteria, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownCriteriaRuntimeException( String aMessage, Criteria aCriteria, Throwable aCause ) {
		super( aMessage, aCriteria, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownCriteriaRuntimeException( String aMessage, Criteria aCriteria ) {
		super( aMessage, aCriteria );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownCriteriaRuntimeException( Criteria aCriteria, Throwable aCause, String aErrorCode ) {
		super( aCriteria, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownCriteriaRuntimeException( Criteria aCriteria, Throwable aCause ) {
		super( aCriteria, aCause );
	}
}
