// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

import static org.refcodes.criteria.CriteriaSugar.*;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

public class CriteriaExamples {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testExpressionQueryFactory1() {
		// @formatter:off
		final Criteria theCriteria = not(
			and(
				not(
					and(
						notEqualWith( "City", "Berlin" ), lessThan( "ZIP", 30000 )
					)
				),
				greaterThan( "AGE", 65 )
			)
		);
		final ExpressionQueryFactory theQueryFactory = new ExpressionQueryFactory();
		final String theQuery = theQueryFactory.fromCriteria( theCriteria );
		System.out.println( "Query = " + theQuery );
		// @formatter:on
	}

	@Test
	public void testExpressionQueryFactory2() {
		// @formatter:off
		final Criteria theCriteria = or(
			not(
				and(
					equalWith( "City", "Berlin" ), equalWith( "ZIP", 10337 )
				)
			),
			greaterOrEqualThan( "Age", 18 )
		);
		final ExpressionQueryFactory theQueryFactory = new ExpressionQueryFactory();
		final String theQuery = theQueryFactory.fromCriteria( theCriteria );
		System.out.println( "Query = " + theQuery );
		// @formatter:on
	}

	@Test
	public void testExpressionCriteriaFactory1() throws ParseException {
		final ExpressionCriteriaFactory theExpressionFactory = new ExpressionCriteriaFactory();
		final ExpressionQueryFactory theQueryFactory = new ExpressionQueryFactory();
		String theQuery = "NOT( NOT( ( City != 'Berlin' ) AND( ZIP < 30000 ) ) AND( AGE > 65 ) )";
		final Criteria theCriteria = theExpressionFactory.fromQuery( theQuery );
		System.out.println( "Schema = " + theCriteria.toSchema() );
		theQuery = theQueryFactory.fromCriteria( theCriteria );
		System.out.println( "Query = " + theQuery );
	}
}
