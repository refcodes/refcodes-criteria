package org.refcodes.criteria;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.ReplaceTextBuilder;

public class ExpressionCriteriaFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testQueries() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Testing the expression criteria factory:".toUpperCase() );
		}
		final ExpressionCriteriaFactory theExpressionFactory = new ExpressionCriteriaFactory();
		final ExpressionQueryFactory theQueryFactory = new ExpressionQueryFactory();
		Criteria eCriteria;
		String eQuery;
		String eAssert;
		for ( int i = 0; i < QueryFixure.QUERIES.length; i++ ) {
			eCriteria = theExpressionFactory.fromQuery( QueryFixure.QUERIES[i] );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "   QUERY[" + i + "] := " + QueryFixure.QUERIES[i] );
			}
			eQuery = theQueryFactory.fromCriteria( eCriteria );
			eAssert = new ReplaceTextBuilder().withText( eQuery ).withFindText( "IS NOT" ).withReplaceText( "!=" ).toString();
			eAssert = new ReplaceTextBuilder().withText( eAssert ).withFindText( "IS" ).withReplaceText( "=" ).toString();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "CRITERIA[" + i + "] := " + eQuery );
			}
			assertEquals( QueryFixure.QUERIES[i].replaceAll( "\"", "'" ), eAssert );
		}
	}
}