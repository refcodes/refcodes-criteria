package org.refcodes.criteria;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.criteria.CriteriaSugar.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class ExpressionQueryFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testExpressionQueryFactory() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Testing the expression query factory:".toUpperCase() );
		}
		Criteria theCriteria = not( and( not( and( notEqualWith( "City", "Berlin" ), lessThan( "ZIP", 30000 ) ) ), greaterThan( "AGE", 65 ) ) );
		ExpressionQueryFactory theQueryFactory = new ExpressionQueryFactory();
		String theQuery = theQueryFactory.fromCriteria( theCriteria );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "EXPECTED := " + QueryFixure.QUERIES[0] );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "   QUERY := " + theQuery );
		}
		assertEquals( QueryFixure.QUERIES[0], theQuery );
		theCriteria = or( not( and( equalWith( "City", "Berlin" ), equalWith( "ZIP", 10337 ) ) ), greaterOrEqualThan( "Age", 18 ) );
		theQueryFactory = new ExpressionQueryFactory();
		theQuery = theQueryFactory.fromCriteria( theCriteria );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "EXPECTED := " + QueryFixure.QUERIES[1] );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "   QUERY := " + theQuery );
		}
		assertEquals( QueryFixure.QUERIES[1], theQuery );
	}
}
