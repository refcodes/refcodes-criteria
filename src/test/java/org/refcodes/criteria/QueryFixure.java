// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.criteria;

/**
 * The Interface QueryFixure.
 *
 * @author steiner
 */
public interface QueryFixure {

	// @formatter:off
	String[] QUERIES = new String[] {
		"NOT ( NOT ( ( City != 'Berlin' ) AND ( ZIP < 30000 ) ) AND ( AGE > 65 ) )",
		"NOT ( ( City = 'Berlin' ) AND ( ZIP = 10337 ) ) OR ( Age >= 18 )",
		"NOT ( ( City = 'Berlin' ) AND ( ZIP = '01320' ) ) OR ( Age >= 18 )",
		"NOT ( ( Gender = 'Male' ) AND ( Size >= 1.9 ) ) OR ( ( Gender = 'Female' ) AND ( Size >= 1.7 ) )",
		"FILE = NULL",
		"FILE != NULL",
		"NOT ( FILE = NULL )",
		"FILE = 'Johanna'",
		"FILE = '   Spaceman   '",
		"QUERY = '>='", 
		"( ( ( FILE = 'D-O-A' ) OR ( FILE = 'Accept' ) ) AND ( GENRE = \"Heavy Metal\" ) ) OR ( GENRE = 'Metal' )",
		"( ( ( FILE != 'D-O-A' ) OR ( FILE = 'Accept' ) ) AND ( GENRE = 'Heavy Metal' ) ) OR ( GENRE = 'Metal' )",
		"( ( ( NOT ( AGE >= 0 ) OR ( AGE <= 18 ) ) AND ( FSK = 12 ) ) OR ( FSK > 16 ) ) OR ( FSK < 18 )",
		"( TAG = 'August' ) INTERSECTION ( TAG = 'Monday' )"
	};
	// @formatter:on
}
